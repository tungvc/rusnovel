import 'package:flutter/material.dart';

enum ThemeEnums { Light, Dark, Summer, Winter }

class ThemeUtils {
  static ThemeData _kLightTheme = new ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.blue,
//    primaryColor: Colors.white,
    fontFamily: "OpenSans",
    backgroundColor: Colors.white,
  );

  static ThemeData _kDarkTheme = new ThemeData(
    brightness: Brightness.dark,
    primarySwatch: Colors.blueGrey,
    fontFamily: "OpenSans",
    //primaryColor: Colors.blueGrey,
    //backgroundColor: Colors.blueGrey,
  );

  static ThemeData _kSummerTheme = new ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.amber,

//      primaryColorBrightness: Brightness.dark,
//    primaryColorLight: Colors.amber[50],
    primaryColor: Colors.amber[50],
    accentColor: Colors.black,
    scaffoldBackgroundColor: Colors.amber[50],
    backgroundColor: Colors.black45,
    fontFamily: "OpenSans",
    primaryColorLight: Colors.black26,
    cardColor: Colors.amber[50],

    // backgroundColor: Colors.yellow[900],
  );

  static ThemeData _kWinterTheme = new ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.brown,

//      primaryColorBrightness: Brightness.dark,
//    primaryColorLight: Colors.amber[50],
    primaryColor: Colors.brown[50],
    accentColor: Colors.black,
    scaffoldBackgroundColor: Colors.brown[50],
    backgroundColor: Colors.black45,
    fontFamily: "OpenSans",
    primaryColorLight: Colors.black26,
    cardColor: Colors.brown[50],

    // brightness: Brightness.light,
    // primarySwatch: Colors.brown,
    // primaryColor: Colors.brown[50],
    // accentColor: Colors.blueGrey,
    // backgroundColor: Colors.black45,
    // fontFamily: "Ranga",
    // primaryColorDark: Colors.black26,
  );
  static Map<ThemeEnums, ThemeData> _themeMap = {
    ThemeEnums.Light: _kLightTheme,
    ThemeEnums.Dark: _kDarkTheme,
    ThemeEnums.Summer: _kSummerTheme,
    ThemeEnums.Winter: _kWinterTheme,
  };

  static ThemeEnums enumsFormString(String str) {
    print(str);
    for (ThemeEnums enu in ThemeEnums.values) {
      if (str == enu.toString()) return enu;
    }

    return ThemeEnums.Light;
  }

  static ThemeData getThemeFromCode(String code) {
    return _themeMap[code];
  }

  static ThemeData getTheme(ThemeEnums code) {
    return _themeMap[code];
  }
}
