import 'package:timeago/timeago.dart';

class DateTimeUtils {
  //final String Dateformat = "yyyyMMdd HH:mm:ss";

  static String getTimePass(int time) {
    if (time < 1) {
      return "N/A";
    }
    final date = new DateTime.fromMillisecondsSinceEpoch(time * 1000);
    final now = new DateTime.now();

    return timeAgo(now.subtract(now.difference(date)));
  }

//  static String formatDate(var Date, String DateFormate);
}
