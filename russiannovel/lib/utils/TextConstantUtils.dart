import 'package:russiannovel/models/enums/ScreenEnums.dart';

class TextConstantUtils {
  static Map<ScreenEnums, String> titleMap = {
    ScreenEnums.HotScreen: "Hot",
    ScreenEnums.SuggestScreen: "Suggest",
    ScreenEnums.SubscribeScreen: "Subscribed",
    ScreenEnums.DownloadedScreen: "Downloaded",
    ScreenEnums.RecentScreen: "Recent",
    ScreenEnums.HomeScreen: "Light Novel",
  };

  static getTitle(ScreenEnums screen) {
    return titleMap[screen];
  }
}
