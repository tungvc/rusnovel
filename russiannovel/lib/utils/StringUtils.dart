
class StringUtils {
  static final Map<String, String> parseReference = {
    "<p />": "\n  ",
    "<p/>": "\n  ",
    "<br>": "\n  ",
    "<br />": "\n  ",
    "&apos;": "'",
  };

  static String getShortString(String source, int length) {
    source = source.trim();
    if (source.length < length) {
      return source;
    } else {
      return source.substring(0, length - 1) + "...";
    }
  }


  static String html2str(String contentRaw) {
    String ret = contentRaw;
    //print('_parseContent raw $contentRaw');
    if (contentRaw != null) {
      parseReference.forEach((key, value) {
        //print('$key and $value');
        ret = ret.replaceAll(key, value);
      });
    }
    ret += "\n\n\n\n\n\n\n\n\n\n\n\n\n";

    //print('_parseContent ret: $ret');
    return ret;
  }

  static String html2strWithHtmlSize(String contentRaw, String h) {
    print('_parseContent ret: $h');
    String ret = contentRaw;
    RegExp reExp = new RegExp("<img (.*?)>");

    Iterable<Match> matches = reExp.allMatches(ret);
    for (Match m in matches) {
      String match = m.group(0);
      ret = ret.replaceFirst(match, '</$h>$match<$h>');
    }

    return '<$h>$ret</$h>';
  }
}
