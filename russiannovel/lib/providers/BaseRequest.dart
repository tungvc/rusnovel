import 'dart:async';
import 'dart:convert';
import 'dart:io';

class BaseRequest {
  Future get(String host, int port, String path) async {
    var httpClient = new HttpClient();
    httpClient.idleTimeout = const Duration(seconds: 5);
    var completer = new Completer();
    try {
      // var uri = new Uri.http(
      //     url, path, params);
      HttpClientRequest request = await httpClient.get(host, port, path);
      HttpClientResponse response =
          await request.close().timeout(const Duration(seconds: 2));

      if (response.statusCode == HttpStatus.ok) {
        var responseBody = await response.transform(utf8.decoder).join();
        //print('reponse $responseBody');
        //print("run here1 $responseBody");
        Map<String, dynamic> data = json.decode(responseBody);
        completer.complete(data);
      } else {
        print('Error getting IP address:\nHttp status ${response.statusCode}');
        completer.completeError(response.statusCode);
      }
    } on TimeoutException catch (_) {
      // A timeout occurred.
      throw new Exception("'BaseRequest timeout ex'");
//      print('BaseRequest timeout ex');
    } on SocketException catch (_) {
      // Other exception
      throw new Exception("'BaseRequest socket ex'");
//      print('BaseRequest socket ex');
    } catch (e) {
      print('Failed getting IP address ' + e.toString());
      completer.completeError(-1);
    }

    return completer.future;
  }
}
