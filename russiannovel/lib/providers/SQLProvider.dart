import 'dart:async';
import 'dart:io';

import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/providers/SQLDataAccess/AppConfigDA.dart';
import 'package:russiannovel/providers/SQLDataAccess/RecentStoryDA.dart';
import 'package:russiannovel/providers/SQLDataAccess/NovelKVDA.dart';
import 'package:russiannovel/providers/SQLDataAccess/SubscribeStoryDA.dart';
import 'package:russiannovel/providers/SQLDataAccess/DownloadedDA.dart';
import 'package:russiannovel/providers/SQLDataAccess/ChapDownloadProcessDA.dart';
import 'package:path_provider/path_provider.dart';
import "package:sqflite/sqflite.dart";

class SQLProvider {
  static final SQLProvider instance = new SQLProvider();

  Database database;

  Future open(success()) async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + AppConfig.sqlPath;
//    database = await deleteDatabase(path);
    database = await openDatabase(path, version: 2, onCreate: onCreate);
    success();
  }

  Future close() async => database.close();

  void onCreate(Database db, int version) async {
    await db.execute('''
          create table ${RecentStoryDA.tableName}(
            ${RecentStoryDA.columnID} integer primary key not null,
            ${RecentStoryDA.columnTitle} text null,
            ${RecentStoryDA.columnUrlt} text null,
            ${RecentStoryDA.columnChapTitle} text null,
            ${RecentStoryDA.columnChapID} integer null,
            ${RecentStoryDA.columnLtRead} integer null
          )
          ''');

    await db.execute('''
          create table ${AppConfigDA.tableName}(
            id integer primary key,
            ${AppConfigDA.columnFontSize} text not null,
            ${AppConfigDA.columnPaperColor} integer null,
            ${AppConfigDA.columnTheme} integer null,
            ${AppConfigDA.columnTextColor} integer null,
            ${AppConfigDA.columnBgColor} integer null
          )
          ''');

    await db.execute('''
          create table ${NovelKVDA.tableName}(
            ${NovelKVDA.columnKey} text primary key not null,
            ${NovelKVDA.columnValue} text null,
            ${NovelKVDA.columnTime} integer null,
            ${NovelKVDA.columnType} text null
          )
          ''');

    await db.execute('''
          create table ${SubscribeStoryDA.tableName}(
            ${SubscribeStoryDA.columnID} integer primary key not null,
            ${SubscribeStoryDA.columnData} text null
          )
          ''');

    await db.execute('''
          create table ${DownloadedDA.tableName}(
            ${DownloadedDA.columnID} integer primary key not null,
            ${DownloadedDA.columnData} text null
          )
          ''');

    await db.execute('''
          create table ${ChapDownloadProcessDA.tableName}(
            ${ChapDownloadProcessDA.columnID} integer primary key not null,
            ${ChapDownloadProcessDA.columnDATA} integer null
          )
          ''');
  }
}
