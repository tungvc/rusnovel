
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/providers/SQLDataAccess/ChapDownloadProcessDA.dart';

class StaticData {
  //static List<GenreEntity> listGenre = new List();
  static List<StoryMiniEntity> listStoryMiniInfoUpdate = new List();
  static bool enableBackgroundDownload = true;
  static List<Map> listMapDownload = new List();
  static int idx = 0;

  static loadListMapDownload() async {
    idx = 0;
    await ChapDownloadProcessDA.instance.select10().then((onvalue) {
      if (onvalue == null || onvalue.isEmpty) {
        enableBackgroundDownload = false;
        return;
      }
      print("size chap list ${onvalue.length}");
      listMapDownload = onvalue;
      idx = 0;
      enableBackgroundDownload = true;
    });
  }

  static downChap() async{
    if (idx < listMapDownload.length) {
      Map map = listMapDownload.elementAt(idx);
      int chapID = map["cid"];
      int storyID = map["storyID"];
      
      await ChapDownloadProcessDA.instance.delete(chapID);
      NovelRequest.instance
          .getChapContent(storyID, chapID, (success) {}, (error) {});
      idx++;
    } else {
      loadListMapDownload();
    }
  }
}
