import 'dart:async';

import 'package:firebase_admob/firebase_admob.dart';

class NovelFirebaseAdMob {
  BannerAd _bannerAd;
  final String _appID='ca-app-pub-4966761461781541~5713894982';
  final String _bannerADId = BannerAd.testAdUnitId;
  static NovelFirebaseAdMob instance = new NovelFirebaseAdMob();
  Function fnListenerClientEvent;
  Function fnListenerFailedToLoad;
  Function fnListenerFailedToShow;

  void init() {
    FirebaseAdMob.instance
        .initialize(appId: _appID);
  }

  BannerAd _createBannerAdShow() {
    return new BannerAd(
        adUnitId: _bannerADId,
        size: AdSize.banner,
        listener: (MobileAdEvent event) {
          print("BannerAd event click $event");
          if (event.index == MobileAdEvent.leftApplication.index) {
            fnListenerClientEvent();
          } else if (event.index == MobileAdEvent.failedToLoad.index) {
            fnListenerFailedToLoad();
          }
        }
        // //targetingInfo: targetingInfo,

        );
  }

  Future loadBanner() async {
    _bannerAd ??= _createBannerAdShow();
    try {
      bool b = await _bannerAd.load();
      if (b) {
        print("load banner ad succ");
      } else {
        print("load banner ad fail!");
      }
    } catch (ex) {
      print("loadBanner ex!");
    }
  }

  void showBanner() {
    print("show banner");
    //loadBanner();
    if (_bannerAd == null) {
      loadBanner();
    }
    _bannerAd.show().then((onValue) {}).catchError((error) {
      fnListenerFailedToShow();
    });
  }

  Future removeBanner() async {
    try {
      await _bannerAd?.dispose();
      await loadBanner();
    } catch (ex) {
      print("AdBanner dispose ex!");
    }
  }
}
