import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

class NovelFirebaseAnalytic {
  static FirebaseAnalytics analytics = new FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      new FirebaseAnalyticsObserver(analytics: analytics);

  

  static Future<Null> setCurrentScreen(String screenNameApp, String scressClassOverrideApp) async {
    await analytics.setCurrentScreen(
      screenName: screenNameApp,
      screenClassOverride: scressClassOverrideApp,
    );
    
  }

}
