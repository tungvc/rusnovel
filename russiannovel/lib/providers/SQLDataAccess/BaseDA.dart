import 'dart:async';
import 'dart:io';

import 'package:russiannovel/config/AppConfig.dart';
import 'package:path_provider/path_provider.dart';
import 'package:russiannovel/providers/SQLProvider.dart';
import "package:sqflite/sqflite.dart";

class BaseDA {

  Database database;

  Future open() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + AppConfig.sqlPath;
    database = await openDatabase(path, version: 1, onCreate: SQLProvider.instance.onCreate);
  }

  // Future<Database> get database async {
  //   if (_database != null) {
  //     return _database;
  //   }
  //   _database = await open();
  //   return _database;
  // }

  Future close() async => database.close();

  
}
