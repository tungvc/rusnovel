import 'package:russiannovel/config/AppConfig.dart';
import "package:russiannovel/providers/SQLDataAccess/BaseDA.dart";
import 'package:russiannovel/providers/SQLProvider.dart';
import "dart:async";

import 'package:russiannovel/utils/ThemeUtils.dart';

class AppConfigDA extends BaseDA {
  static final String tableName = "AppConfig";
  static final String columnFontSize = "fontSize";
  static final String columnPaperColor = "paperColor";
  static final String columnTheme = "theme";
  static final String columnTextColor = "textColor";
  static final String columnBgColor = "bgColor";

  AppConfigDA() {
    this.database = SQLProvider.instance.database;
  }
  static final AppConfigDA instance = new AppConfigDA();

  Future loadAppConfig(success()) async {
    bool ret = false;
    try {
      //await open();

      List<Map> maps = await database.query(tableName,
          columns: [
            columnFontSize,
            columnPaperColor,
            columnTheme,
            columnTextColor,
            columnBgColor
          ],
          where: "id = ?",
          whereArgs: [1]);
      //close();
      if (maps.length > 0) {
        print("---------------load success---------");

        AppConfig.fontSize = double.parse(maps.first["$columnFontSize"]);
        AppConfig.paperColor = maps.first["$columnPaperColor"];
        AppConfig.theme = ThemeUtils.enumsFormString(maps.first["$columnTheme"]);
        AppConfig.textChapReaderColor = maps.first["$columnTextColor"];
        AppConfig.backgroundChapReaderColor = maps.first["$columnBgColor"];
        AppConfig.isInsert = false;
        ret = true;
      }
    } catch (err) {
      print("loadAppConfig err $err");
    }
    success();
    return ret;
    
  }

  Future<bool> saveAppConfig() async {
    try {
      //await open();
      int id = await database.insert(tableName, AppConfig.toMap());
      print("save config success");
      return id > 0;
    } catch (exception) {
      try {
        int id = await database.update(tableName, AppConfig.toMap());
        print("update config success");
        return id > 0;
      } catch (exception) {
        print(exception);
        return false;
      }
    }

  }

}
