import "package:sqflite/sqflite.dart";
import "package:russiannovel/providers/SQLDataAccess/BaseDA.dart";
import 'package:russiannovel/providers/SQLProvider.dart';
import "dart:async";

class NovelKVData {
  final String nKey;
  final String nValue;
  final int updateTime;
  final String ntype;

  const NovelKVData({this.nKey, this.nValue, this.updateTime, this.ntype});

  factory NovelKVData.fromJson(Map<String, dynamic> json) {
    return new NovelKVData(
      nKey: json['nKey'],
      nValue: json["nValue"],
      updateTime: json["updateTime"],
      ntype: json['ntype'],
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      "nKey": nKey,
      "nValue": nValue,
      "updateTime": updateTime,
      "ntype": ntype,
    };
    return map;
  }
}

class NovelKVDA extends BaseDA {
  static final String tableName = "NovelKVDA";
  static final String columnKey = "nKey";
  static final String columnValue = "nValue";
  static final String columnTime = "updateTime";
  static final String columnType = "ntype";

  static final NovelKVDA instance = new NovelKVDA();

  NovelKVDA() {
    this.database = SQLProvider.instance.database;
  }

  Database database;

  Future<dynamic> insert(NovelKVData entity) async {
     return await database.insert(tableName, entity.toMap());
     
  }

  Future<dynamic> update(NovelKVData entity)  async {
    return await database.update(tableName, entity.toMap(),where: "$columnKey = ?", whereArgs: [entity.nKey]);
    
  }

  Future<NovelKVData> selectById(String nKey) async {
    List<Map> maps = await database.query(tableName,
        columns: [columnKey, columnValue, columnTime],
        where: "$columnKey = ?",
        whereArgs: [nKey]);
    if (maps.length > 0) {
      return new NovelKVData.fromJson(maps.first);
    }
    return null;
  }

  Future<NovelKVData> selectByIdAndType(String nKey, String mType) async {
    List<Map> maps = await database.query(tableName,
        columns: [columnKey, columnValue, columnTime],
        where: "$columnKey = ? AND $columnType = ?",
        whereArgs: [nKey, mType]);
    if (maps.length > 0) {
      return new NovelKVData.fromJson(maps.first);
    }
    return null;
  }

  Future<List<Map>> selectByType(String mType) async {
    List<Map> maps = await database.query(tableName,
        columns: [columnKey, columnValue, columnTime],
        where: "$columnType = ?",
        whereArgs: [mType]);
    return maps;
  }



  void updateChap(int storyID, int chapID, String jsonRequest) async {
    NovelKVData kvData = new NovelKVData(
        nKey: "CHAP_" + chapID.toString(),
        nValue: jsonRequest,
        updateTime: (new DateTime.now().millisecondsSinceEpoch),
        ntype: "STORY_" + storyID.toString());

    this.insert(kvData).catchError((error) {
      update(kvData);
    });
  }

  void updateStory(int storyID, String jsonRequest) async {
    NovelKVData kvData = new NovelKVData(
        nKey: "STORY_" + storyID.toString(),
        nValue: jsonRequest,
        updateTime: (new DateTime.now()).millisecondsSinceEpoch,
        ntype: "STORY");

    this.insert(kvData).catchError((error) {
      update(kvData);
    });
  }

  void updateCategory(String cate, String jsonRequest) async {
    print("update category");
    NovelKVData kvData = new NovelKVData(
        nKey: "CATE_" + cate, nValue: jsonRequest, updateTime: (new DateTime.now()).millisecondsSinceEpoch, ntype: "CATE");

    this.insert(kvData).catchError((error) {
      update(kvData);
    });
    
  }

  void updateGenre(String jsonRequest) async {
    print("update category");
    NovelKVData kvData = new NovelKVData(
        nKey: "GENRE", nValue: jsonRequest, updateTime: (new DateTime.now()).millisecondsSinceEpoch, ntype: "GENRE");

    this.insert(kvData).catchError((error) {
      update(kvData);
    });
    
  }
}
