import "dart:async";

import 'package:russiannovel/providers/SQLDataAccess/BaseDA.dart';
import 'package:russiannovel/providers/SQLDataAccess/RecentStoryData.dart';
import 'package:russiannovel/providers/SQLProvider.dart';

class RecentStoryDA extends BaseDA {
  static final String tableName = "RecentStory";
  static final String columnID = "cid";
  static final String columnTitle = "title";
  static final String columnUrlt = "urlt";
  static final String columnChapTitle = "chapTitle";  // chapTitle wich reading 
  static final String columnChapID = "chapID";   // chap Id which reading
  static final String columnLtRead = "ltRead";
  

  RecentStoryDA() {
    this.database = SQLProvider.instance.database;
  }
  static final RecentStoryDA instance = new RecentStoryDA();

  Future<int> saveRecentStory(
      int id, String title, String urlt, int chapId, String chapTitle) async {
    try {
      //await open();
      int ret;
      RecentStoryData ent = await selectByID(id);
      if (ent == null) {
        Map<String, dynamic> map = {
          columnID: id,
          columnTitle: title,
          columnUrlt: urlt,
          columnChapTitle: chapTitle,
          columnChapID:chapId,
          columnLtRead: new DateTime.now().millisecondsSinceEpoch,
        };
        ret = await database.insert(tableName, map);
      } else {
        ret = await updateRecentStory(id, chapTitle, chapId);
      }

      //close();
      return ret;
    } catch (err) {
      print("save err $err");
      return -1;
    }
  }

  Future<List<Map>> selectRecentStory() async {
    //await open();
    List<Map> maps = await database
        .rawQuery("SELECT * FROM $tableName ORDER BY $columnLtRead DESC");
    return maps;
  }

  Future<RecentStoryData> selectByID(int id) async {
    List<Map> maps = await database
        .rawQuery("SELECT * FROM $tableName where $columnID = $id");
    RecentStoryData ret;
    if (maps != null && maps.length > 0) {
      ret = new RecentStoryData.fromJson(maps[0]);
    }
    return ret;
  }

  Future<int> updateRecentStory(int id, String chapTitle, int chapID) async {
    Map<String, dynamic> map = {
      columnChapTitle: chapTitle,
      columnChapID:chapID,
      columnLtRead: new DateTime.now().millisecondsSinceEpoch,
    };

    int ret =
        await database.update(tableName, map, where: "$columnID = ?", whereArgs: [id]);

    return ret;
  }
}
