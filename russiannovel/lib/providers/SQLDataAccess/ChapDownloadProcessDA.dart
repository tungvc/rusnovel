import "dart:async";
import 'package:russiannovel/providers/SQLProvider.dart';
import "package:russiannovel/providers/SQLDataAccess/BaseDA.dart";


class ChapDownloadProcessDA extends BaseDA {
  static final String tableName = "ChapDownProcess";
  static final String columnID = "cid";
  static final String columnDATA = "storyID";

  static final ChapDownloadProcessDA instance = new ChapDownloadProcessDA();

  ChapDownloadProcessDA() {
    this.database = SQLProvider.instance.database;
  }

  Future<int> insert(int cid, int storyID) async {
    try {
      
      Map<String, dynamic> map = {
        columnID: cid,
        columnDATA: storyID,
      };
      int id = await database.insert(tableName, map);
      return id;
    } catch (err) {
      print("insert downloaded $err");
      return -1;
    }
  }


  Future<List<Map>> select10() async {
    try {
      
      List<Map> maps = await database.rawQuery("SELECT * FROM $tableName LIMIT 0,10");
      return maps;
      
    } catch (ex) {
      print("select all subscribe $ex");
      return null;
    }
  }

  Future<int> delete(int cid) async {
    try {
      return await database
          .delete(tableName, where: "$columnID = ?", whereArgs: [cid]);
    } catch (ex) {
      print("delete subscribe $ex");
      return -1;
    }
  }

}