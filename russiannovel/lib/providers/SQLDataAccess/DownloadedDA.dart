import "dart:async";
import "dart:convert";
import 'package:russiannovel/providers/SQLProvider.dart';
import "package:russiannovel/providers/SQLDataAccess/BaseDA.dart";
import "package:russiannovel/models/entity/StoryMiniEntity.dart";

class DownloadedDA extends BaseDA {
  static final String tableName = "DownloadedStory";
  static final String columnID = "cid";
  static final String columnData = "data";

  static final DownloadedDA instance = new DownloadedDA();

  DownloadedDA() {
    this.database = SQLProvider.instance.database;
  }

  Future<int> insert(StoryMiniEntity entity) async {
    try {
      var encoded = jsonEncode(entity.toMap());
      Map<String, dynamic> map = {
        columnID: entity.cid,
        columnData: encoded,
      };
      int id = await database.insert(tableName, map);
      return id;
    } catch (err) {
      print("insert downloaded $err");
      return -1;
    }
  }

  Future<StoryMiniEntity> selectById(int cid) async {
    List<Map> maps = await database.query(tableName,
        columns: [columnID, columnData],
        where: "$columnID = ?",
        whereArgs: [cid]);
    if (maps.length > 0) {
      return new StoryMiniEntity.fromJson(maps.first[columnData]);
    }
    return null;
  }

  Future<bool> beDownloaded(int cid) async {
    List<Map> maps = await database.query(tableName,
        columns: [columnID], where: "$columnID = ?", whereArgs: [cid]);
    if (maps.length > 0) {
      return true;
    }
    return false;
  }

  Future<List<StoryMiniEntity>> selectAll() async {
    try {
      List<Map> maps = await database.rawQuery("SELECT * FROM $tableName");

      List<StoryMiniEntity> listStory = [];
      maps.forEach((map) {
        Map<String, dynamic> json = jsonDecode(map[columnData]);
        print(json);
        listStory.add(new StoryMiniEntity.fromJson(json));
      });

      return listStory;
    } catch (ex) {
      print("select all subscribe $ex");
      return null;
    }
  }

  Future<int> delete(int cid) async {
    try {
      return await database
          .delete(tableName, where: "$columnID = ?", whereArgs: [cid]);
    } catch (ex) {
      print("delete subscribe $ex");
      return -1;
    }
  }

  Future<int> update(StoryMiniEntity entity) async {
    return await database.update(tableName, entity.toMap(),
        where: "$columnID = ?", whereArgs: [entity.cid]);
  }
}
