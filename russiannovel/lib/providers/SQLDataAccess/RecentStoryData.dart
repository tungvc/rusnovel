class RecentStoryData implements Comparable<RecentStoryData> {
  final int cid;
  final String title ;
  final String urlt ;
  final String chapTitle;  // chapTitle wich reading 
  final int chapID ;   // chap Id which reading
  final int ltRead ;
  

  const RecentStoryData(
      {this.cid, this.title, this.urlt, this.chapTitle, this.chapID, this.ltRead});

  factory RecentStoryData.fromJson(Map<String, dynamic> json) {
    return new RecentStoryData(
      cid: json['cid'],
      title: json["title"],
      urlt: json["urlt"],
      chapTitle: json["chapTitle"],
      chapID: json["chapID"],
      ltRead: json['ltRead'],
    );
  }

  Map<String,dynamic> toMap(){
    Map<String,dynamic> map = {
      "cid": cid,
      "title":title,
      "urlt":urlt,
      "chapTitle":chapTitle,
      "chapID":chapID,
      "ltRead":ltRead,
    };
    return map;
  }

  @override
  int compareTo(RecentStoryData other) => cid.compareTo(other.cid);
}
