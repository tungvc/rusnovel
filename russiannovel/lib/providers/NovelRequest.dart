import 'dart:core';
import 'dart:async';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'BaseRequest.dart';
//import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/models/response/GetConfigAppResponse.dart';
import 'package:russiannovel/models/response/GetGenreResponse.dart';
import 'package:russiannovel/models/response/GetNovelByCategoryResponse.dart';
import 'package:russiannovel/models/response/GetChapContentResponse.dart';
import 'package:russiannovel/models/response/SearchResponse.dart';
import 'package:russiannovel/models/response/GetNovelDetailResponse.dart';
import 'package:russiannovel/providers/SQLDataAccess/NovelKVDA.dart';

class NovelRequest extends BaseRequest {
  static NovelRequest instance = new NovelRequest();

  final String host = "167.179.65.238";
  final int port = 80;
  final String pathData = "mapi/data";
  final String pathStatic = "mapi/static";

  void getConfigApp(
      success(GetConfigAppResponse response), error(dynamic error)) {
    Map params = new Map();
    params['method'] = 'data_getconfigapp';
    this.setCommonParam(params);
    String strParam = buildPath(pathData, params);
    Future futureMap = this.get(host, port, strParam);
    futureMap.then((resValue) {
      GetConfigAppResponse response =
          new GetConfigAppResponse.fromJson(resValue);
      success(response);
    }).catchError((error) {
      error(error);
    });
  }

  void getNovelByCategory(String categoryKey, int idx, int length,
      success(GetNovelByCategoryResponse response), error(dynamic error)) {
    Map params = new Map();
    params['method'] = 'data_getbycategory';
    params['cate'] = categoryKey;
    params['idx'] = idx.toString();
    params['length'] = length.toString();
    this.setCommonParam(params);
    String strParam = buildPath(pathData, params);
    Future futureMap = this.get(host, port, strParam);
    futureMap.then((resValue) {
      GetNovelByCategoryResponse response =
          new GetNovelByCategoryResponse.fromJson(resValue);
      success(response);
      if (idx == 0) {
        NovelKVDA.instance.updateCategory(categoryKey, json.encode(resValue));
      }
    }).catchError((error) {
      if (idx == 0) {
        NovelKVDA.instance
            .selectById("CATE_" + categoryKey)
            .then((NovelKVData kvdata) {
          GetNovelByCategoryResponse response =
              new GetNovelByCategoryResponse.fromJson(
                  json.decode(kvdata.nValue));
          success(response);
        }).catchError((onError) {
          print('get data error');
          error(error);
        });
      } else {
        error(error);
      }
    });
  }

  void getNovelDetail(int storyID, success(GetNovelDetailResponse response),
      fail(dynamic error)) {
    Map params = new Map();
    params['method'] = 'data_getdetail';
    params['storyid'] = storyID.toString();
    this.setCommonParam(params);
    String strParam = buildPath(pathData, params);
    Future future = get(host, port, strParam);
    future.then((res) {
      GetNovelDetailResponse response =
          new GetNovelDetailResponse.fromJson(res);
      success(response);
      NovelKVDA.instance.updateStory(storyID, json.encode(res));
    }).catchError((err) {
      NovelKVDA.instance
          .selectById("STORY_" + storyID.toString())
          .then((NovelKVData kvdata) {
        GetNovelDetailResponse response =
            new GetNovelDetailResponse.fromJson(json.decode(kvdata.nValue));
        success(response);
      }).catchError((onError) {
        fail(err);
      });
    });
  }

  void getChapContent(int storyID, int chapID,
      onSuccessFunc(GetChapContentResponse response), onErrorFunc(dynamic error)) {
    Map params = new Map();
    params['method'] = 'data_chapcontent';
    params['chapid'] = chapID.toString();
    params['storyid'] = storyID.toString();
    this.setCommonParam(params);
    String strParam = buildPath(pathData, params);
    Future futureMap = this.get(host, port, strParam);
    futureMap.then((response) {
      GetChapContentResponse gccres =
          new GetChapContentResponse.fromJson(response);
      //update db
      onSuccessFunc(gccres);
      String strJson = json.encode(response);
      //print('toJson $strJson');
      NovelKVDA.instance.updateChap(storyID, chapID, strJson);
    }).catchError((error) {
      NovelKVDA.instance
          .selectById("CHAP_" + chapID.toString())
          .then((NovelKVData kvdata) {
        GetChapContentResponse response =
            new GetChapContentResponse.fromJson(json.decode(kvdata.nValue));

        print("content ${response.data.content}");
        if (response == null)
          onErrorFunc(error);
        else
          onSuccessFunc(response);
      }).catchError((onError) {
        print("qskjergliwqglkrgslkfdgkgsflkgewqlrgewq");
        onErrorFunc(onError);
      });
    });
  }

  void search(
      String text, int idx, success(SearchResponse res), fail(dynamic err)) {
    Map params = new Map();
    params['method'] = 'data_search';
    params['text'] = text;
    params['idx'] = idx;

    this.setCommonParam(params);
    String strParam = buildPath(pathData, params);
    Future furtureMap = this.get(host, port, strParam);
    furtureMap.then((response) {
      SearchResponse res = new SearchResponse.fromJson(response);
      success(res);
    }).catchError((error) {
      fail(error);
    });
  }

  void reportError(
      String message) {
    Map params = new Map();
    params['method'] = 'data_reporterror';
    params['message'] = message;
    
    this.setCommonParam(params);
    String strParam = buildPath(pathData, params);
    Future furtureMap = this.get(host, port, strParam);
    furtureMap.then((response) {
      
    }).catchError((error) {
      
    });
  }

  void setCommonParam(Map params) {
    DateTime dateTime = new DateTime.now();
    String sigRaw = dateTime.microsecondsSinceEpoch.toString() + "@456!STORYRQ";
    var bytes = utf8.encode(sigRaw); // data being hashed
    var digest = md5.convert(bytes);
    params['prequest'] = "STORYRQ";
    params['sig'] = digest.toString();
    params['t'] = dateTime.microsecondsSinceEpoch
        .toString(); //dateTime.millisecond.toString();
    //print("${sigRaw}: md5 ${digest}");
  }

  String buildPath(String basePath, Map params) {
    String ret = basePath + "?";
    params.forEach((key, value) {
      ret += ('$key=$value&');
      //print('$key:$value');//string interpolation in action
    });
    print('path = $ret');
    return ret;
  }

  void getGenre(success(GetGenreResponse response), error(dynamic error)) {
    Map params = new Map();
    params['method'] = 'static_getcategory';
    this.setCommonParam(params);
    String strParam = buildPath(pathStatic, params);
    Future futureMap = this.get(host, port, strParam);
    futureMap.then((resValue) {
      GetGenreResponse response = new GetGenreResponse.fromJson(resValue);
      success(response);
      String strJson = json.encode(resValue);
      //print('toJson $strJson');
      NovelKVDA.instance.updateGenre(strJson);
    }).catchError((error) {
      NovelKVDA.instance.selectById("GENRE").then((NovelKVData kvdata) {
        GetGenreResponse response =
            new GetGenreResponse.fromJson(json.decode(kvdata.nValue));
        success(response);
      }).catchError((onError) {
        error(error);
      });
    });
    return null;
  }
}
