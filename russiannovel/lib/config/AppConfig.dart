
import 'package:flutter/material.dart';
import 'package:russiannovel/constant/BaseDefine.dart';
import 'package:russiannovel/constant/RussianDefine.dart';
import 'package:russiannovel/utils/ThemeUtils.dart';

Map<int, Color> colorMap = {
  0: Colors.white,
  1: Colors.yellow,
};

class AppConfig {
  static bool beShowAds = true;
  static bool lockAPP = false;
  static bool lockDOWN = false;
  static int maxChapDown = 20;
  static int minChapDown = 5;
  static int readChapAds = 2;
  static int ratioDownAll = 50;

  static double fontSize = 15.0;
  static int paperColor = 0;
  static ThemeEnums theme = ThemeEnums.Light;
  static bool useLightTheme = true;
  static String fontFamily = "Roboto";

  static int textChapReaderColor = 4294967295;
  static int backgroundChapReaderColor = 4278190080;

  static double defaultImageWidth = 120.0;

  static bool isInsert = true;
  static final String sqlPath = "lightnovel.db";

  static BaseDefine langDefine = new RussianDefine();

  //text

  static final AppConfig _instance = new AppConfig._internal();

  factory AppConfig() {
    return _instance;
  }

  AppConfig._internal();

  static Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      "id": 1,
      "fontSize": fontSize.toString(),
      "paperColor": paperColor,
      "theme": theme.toString(),
      "textColor": textChapReaderColor,
      "bgColor": backgroundChapReaderColor,
    };
    return map;
  }

  static Color getPaperColor(int idex) {
    return colorMap[idex];
  }
}