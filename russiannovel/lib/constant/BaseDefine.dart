import 'package:russiannovel/models/enums/ScreenEnums.dart';
import 'package:russiannovel/utils/TextConstantUtils.dart';

abstract class BaseDefine {

  String APP_NAME = "Kho Truyện";
  String ALERT_TITLE = "Thông báo";

  //
  String STORY_INFO_READ_MORE_BUTTON_TEXT = "Đọc thêm";
  String STORY_INFO_COLLAPSE_BUTTON_TEXT = "Thu gọn";
  String STORY_INFO_DOWNLOAD_BUTTON_TEXT = "Đã tải về";
  String STORY_INFO_FILTER_HINT_TEXT = "Nhập tên chap";
  String STORY_INFO_AUTHOR_LABEL_TEXT = "Tác giả";
  String STORY_INFO_CATEGORY_LABEL_TEXT = "Thể loại";
  String STORY_INFO_LAST_UPDATE_LABEL_TEXT = "Cập nhật";
  String STORY_INFO_AGREE_CONTINUE_READ_BUTTON_TEXT = "Đọc tiếp";
  String STORY_INFO_DISAGREE_CONTINUE_READ_BUTTON_TEXT =
      "Chọn chap khác";
  String STORY_INFO_CONTINUE_READ_DIALOG_TEXT =
      "Bạn đang đọc chap {{chaptitle}} Bạn có muốn tiếp tục đọc ?";
  String STORY_INFO_DESCRIPTION_LABEL_TEXT = "Tóm tắt";

  String LASTEST_LABEL_TEXT = "Chap Mới Nhất";
  String DRAWER_HOME_LABEL_TEXT = "Truyện Update";
  String DRAWER_HOT_LABEL_TEXT = "Hot";
  String DRAWER_SUGGEST_LABEL_TEXT = "Gợi ý";
  String DRAWER_RECENT_LABEL_TEXT = "Vừa xem";
  String DRAWER_SUBSCRIBE_LABEL_TEXT = "Theo dõi";
  String DRAWER_DOWNLOADED_LABEL_TEXT = "Đã tải về";
  String DRAWER_CATEGORY_LABEL_TEXT = "Thể loại";
  String DRAWER_SETTING_LABEL_TEXT = "Cài đặt";
  String DRAWER_SEARCH_TEXT = "Tìm Kiếm";
  String DRAWER_PRIVACY_POLICY_LABEL_TEXT = "Chính sách";
  String DRAWER_ABOUT_LABEL_TEXT = "Về chúng tôi";
  String DRAWER_EXIT_LABEL_TEXT = "Thoát";
  String DRAWER_SELECT_PAGE_TEXT = "Lựa chọn";

  //
  String SETTING_TITLE = "Cài đặt";
  String SETTING_THEME_GROUP = "Giao diện";
  String SETTING_FONT_SIZE_GROUP = "Cỡ chữ";
  String SETTING_FONT_SIZE_SMALL_TEXT = "Nhỏ";
  String SETTING_FONT_SIZE_NORMAL_TEXT = "Vừa";
  String SETTING_FONT_SIZE_LARGE_TEXT = "Lớn";
  String SETTING_FONT_SIZE_HUGE_TEXT = "Khổng lồ";
  String SETTING_COLOR_GROUP = "Tùy chọn trang đọc";
  String SETTING_TEXT_COLOR = "Màu chữ";
  String SETTING_BACK_GROUND_COLOR = "Màu nền";

  //
  String CHAP_READER_NEXT_CHAP = "Chap sau";
  String CHAP_READER_PREVIOUS_CHAP = "Chap trước";

  //
  String DIALOG_REPORT_CHAP_TITLE = "Gửi Thông Báo Truyện Lỗi";

  //
  String DIALOG_CLOSE_BUTTON_TEXT = "Đóng";
  String DIALOG_CONTACT_US_BUTTON_TEXT = "Liên hệ";
  String DIALOG_LEAVE_COMMENT_BUTTON_TEXT = "Đánh giá";
  String DIALOG_ABOUT_CONTENT_TEXT =
      "Toàn bộ dữ liệu được sưu tầm từ internet . Top Truyện không chịu trách nhiệm về nội dung và bản quyền.";
  String DIALOG_ABOUT_CONTACT_TEXT =
      "Mọi ý kiến đóng góp và yêu cầu truyện vui lòng gửi về hòm thư vncicada@gmail.com";

  String EMAIL_FEEDBACK = "mailto:vncicada@gmail.com?subject=LightNovel";
  String URL_GOOGLE_PLAY =
      "https://play.google.com/store/apps/details?id=com.vcicada.lightnovel";
}
