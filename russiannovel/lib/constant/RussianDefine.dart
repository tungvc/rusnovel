import 'package:russiannovel/constant/BaseDefine.dart';

class RussianDefine extends BaseDefine {
  RussianDefine() {
    APP_NAME = "легкая новелла";
    ALERT_TITLE = "бдительный";

    //
    STORY_INFO_READ_MORE_BUTTON_TEXT = "Больше";
    STORY_INFO_COLLAPSE_BUTTON_TEXT = "Меньше";
    STORY_INFO_DOWNLOAD_BUTTON_TEXT = "загруженный";
    STORY_INFO_FILTER_HINT_TEXT = "глава";
    STORY_INFO_AUTHOR_LABEL_TEXT = "автор";
    STORY_INFO_CATEGORY_LABEL_TEXT= "Каталог";
    STORY_INFO_LAST_UPDATE_LABEL_TEXT= "Опубликовано";
    STORY_INFO_AGREE_CONTINUE_READ_BUTTON_TEXT= "согласна";
    STORY_INFO_DISAGREE_CONTINUE_READ_BUTTON_TEXT= "не соглашаться";
    STORY_INFO_CONTINUE_READ_DIALOG_TEXT=
        "Вы читаете = {{chaptitle}} Хочешь продолжить?";
    STORY_INFO_DESCRIPTION_LABEL_TEXT = "Описание";

    //
    LASTEST_LABEL_TEXT= "самый последний";
    DRAWER_HOME_LABEL_TEXT= "Главная";
    DRAWER_HOT_LABEL_TEXT= "тенденция";
    DRAWER_RECENT_LABEL_TEXT= "недавний";
    DRAWER_SUBSCRIBE_LABEL_TEXT= "подписываться";
    DRAWER_DOWNLOADED_LABEL_TEXT= "загруженный";
    DRAWER_CATEGORY_LABEL_TEXT= "Каталог";
    DRAWER_SETTING_LABEL_TEXT= "настройка";
    DRAWER_ABOUT_LABEL_TEXT= "Около";
    DRAWER_EXIT_LABEL_TEXT= "Выход";
    //
    SETTING_TITLE= "установка";
    SETTING_THEME_GROUP= "тема";
    SETTING_FONT_SIZE_GROUP= "Pазмер текста";
    SETTING_FONT_SIZE_SMALL_TEXT= "маленький";
    SETTING_FONT_SIZE_NORMAL_TEXT= "нормальный";
    SETTING_FONT_SIZE_LARGE_TEXT= "Ларг";
    SETTING_FONT_SIZE_HUGE_TEXT= "огромный";

    //
    CHAP_READER_NEXT_CHAP= "Следующая глава";
    CHAP_READER_PREVIOUS_CHAP= "предыдущая глава";

    //
    DIALOG_CLOSE_BUTTON_TEXT= "близко";
    DIALOG_CONTACT_US_BUTTON_TEXT= "связаться с нами";
    DIALOG_LEAVE_COMMENT_BUTTON_TEXT= "Оставьте комментарий";
    DIALOG_ABOUT_CONTENT_TEXT="Все sours собираются из Интернета. Мы (Light Novel) не несем ответственности за контент и авторские права.";
    DIALOG_ABOUT_CONTACT_TEXT="Если у вас есть какой-либо вклад, просьба или любая проблема при использовании этого приложения, вы можете связаться с нами";

    EMAIL_FEEDBACK = "mailto:vncicada@gmail.com?subject=RussiaNovel";
    URL_GOOGLE_PLAY =
        "https://play.google.com/store/apps/details?id=com.vcicada.russianovel";
  }
}
