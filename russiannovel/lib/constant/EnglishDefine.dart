//import 'package:russiannovel/constant/BaseDefine.dart';
//import 'package:russiannovel/models/enums/ScreenEnums.dart';
//import 'package:russiannovel/utils/TextConstantUtils.dart';
//
//class EnglishDefine extends BaseDefine {
//  static Map<TextConstantEnums, String> defineMap = {
//    TextConstantEnums.APP_NAME: "Light Novel",
//    TextConstantEnums.ALERT_TITLE: "Alert",
//    //
//    TextConstantEnums.STORY_INFO_READ_MORE_BUTTON_TEXT: "Read more",
//    TextConstantEnums.STORY_INFO_COLLAPSE_BUTTON_TEXT: "Collapse",
//    TextConstantEnums.STORY_INFO_DOWNLOAD_BUTTON_TEXT: "Dowload",
//    TextConstantEnums.STORY_INFO_FILTER_HINT_TEXT: "Enter Chap",
//    TextConstantEnums.STORY_INFO_AUTHOR_LABEL_TEXT: "Author",
//    TextConstantEnums.STORY_INFO_CATEGORY_LABEL_TEXT: "Category",
//    TextConstantEnums.STORY_INFO_LAST_UPDATE_LABEL_TEXT: "Last Update",
//    TextConstantEnums.STORY_INFO_AGREE_CONTINUE_READ_BUTTON_TEXT: "AGREE",
//    TextConstantEnums.STORY_INFO_DISAGREE_CONTINUE_READ_BUTTON_TEXT: "DISAGREE",
//    TextConstantEnums.STORY_INFO_CONTINUE_READ_DIALOG_TEXT:
//        "You are reading: {{chaptitle}} Do you want continue ?",
//
//    //
//    TextConstantEnums.LASTEST_LABEL_TEXT: "Lastest",
//    TextConstantEnums.DRAWER_HOME_LABEL_TEXT: "Home",
//    TextConstantEnums.DRAWER_HOT_LABEL_TEXT: "Hot",
//    TextConstantEnums.DRAWER_SUGGEST_LABEL_TEXT: "Suggest",
//    TextConstantEnums.DRAWER_RECENT_LABEL_TEXT: "Recent",
//    TextConstantEnums.DRAWER_SUBSCRIBE_LABEL_TEXT: "Subscribe",
//    TextConstantEnums.DRAWER_DOWNLOADED_LABEL_TEXT: "Downloaded",
//    TextConstantEnums.DRAWER_CATEGORY_LABEL_TEXT: "Category",
//    TextConstantEnums.DRAWER_SETTING_LABEL_TEXT: "Setting",
//    TextConstantEnums.DRAWER_ABOUT_LABEL_TEXT: "About",
//    TextConstantEnums.DRAWER_EXIT_LABEL_TEXT: "Exit",
//    //
//    TextConstantEnums.SETTING_TITLE: "SETTING",
//    TextConstantEnums.SETTING_THEME_GROUP: "Theme",
//    TextConstantEnums.SETTING_FONT_SIZE_GROUP: "Font Size",
//    TextConstantEnums.SETTING_FONT_SIZE_SMALL_TEXT: "Small",
//    TextConstantEnums.SETTING_FONT_SIZE_NORMAL_TEXT: "Normal",
//    TextConstantEnums.SETTING_FONT_SIZE_LARGE_TEXT: "Large",
//    TextConstantEnums.SETTING_FONT_SIZE_HUGE_TEXT: "Huge",
//    TextConstantEnums.SETTING_COLOR_GROUP: "Chap reader",
//    TextConstantEnums.SETTING_TEXT_COLOR: "Text color",
//    TextConstantEnums.SETTING_BACK_GROUND_COLOR: "Background color",
//
//    //
//    TextConstantEnums.CHAP_READER_NEXT_CHAP: "Next chap",
//    TextConstantEnums.CHAP_READER_PREVIOUS_CHAP: "Previous chap",
//
//    //
//    TextConstantEnums.DIALOG_CLOSE_BUTTON_TEXT: "Close",
//    TextConstantEnums.DIALOG_CONTACT_US_BUTTON_TEXT: "Contact us",
//    TextConstantEnums.DIALOG_LEAVE_COMMENT_BUTTON_TEXT: "Leave comment",
//    TextConstantEnums.DIALOG_ABOUT_CONTENT_TEXT:"All sours are collected from the internet. We (Light Novel) do not have responsibility for any content and copyright.",
//    TextConstantEnums.DIALOG_ABOUT_CONTACT_TEXT:"If you have any contribution, request or any proplem when using this app, you can contact us",
//
//    TextConstantEnums.EMAIL_FEEDBACK:"mailto:vncicada@gmail.com?subject=LightNovel",
//    TextConstantEnums.URL_GOOGLE_PLAY:"https://play.google.com/store/apps/details?id=com.vcicada.lightnovel",
//  };
//
//  static Map<ScreenEnums, String> titleMap = {
//    ScreenEnums.HotScreen:"Hot",
//    ScreenEnums.SuggestScreen:"Suggest",
//    ScreenEnums.SubscribeScreen:"Subscribed",
//    ScreenEnums.DownloadedScreen:"Downloaded",
//    ScreenEnums.RecentScreen:"Recent",
//    ScreenEnums.HomeScreen:"Light Novel",
//  };
//
//}
