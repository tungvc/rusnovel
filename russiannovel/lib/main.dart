import 'package:flutter/material.dart';
import 'package:russiannovel/app/LightNovelApp.dart';
import 'package:russiannovel/providers/SQLDataAccess/AppConfigDA.dart';
import 'package:russiannovel/providers/SQLProvider.dart';

void main() {
  SQLProvider.instance.open(() {
    AppConfigDA.instance.loadAppConfig(() {
      runApp(LightNovelApp.getInstance());
    });
  });
}
