
class BaseResponse {
  final int error;
  final String message;
  final int stime;

  const BaseResponse(this.error, this.message, this.stime);

  factory BaseResponse.fromJson(Map<String, dynamic> json) {
    //print('object  $json["error"], $json["message"]');
    return new BaseResponse(json['error'], json['message'], json['stime']);
  }

  static  Map<String, dynamic>  getDataField(Map<String, dynamic> json){
    
      return json['data'];
    
  }
  
}
