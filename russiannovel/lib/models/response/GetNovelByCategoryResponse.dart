import 'BaseResponse.dart';
import '../entity/StoryMiniEntity.dart';

class GetNovelByCategoryResponse extends BaseResponse {
  final GetNovelByCategoryRData data;
  const GetNovelByCategoryResponse(int error, String message, int stime, this.data)
      : super(error, message, stime);

  factory GetNovelByCategoryResponse.fromJson(Map<String, dynamic> json) {
    BaseResponse baseResponse = new BaseResponse.fromJson(json);
    int errorCode = baseResponse.error;
    print('ErrorCode : $errorCode');
    Map<String, dynamic> dataJson = BaseResponse.getDataField(json);
    GetNovelByCategoryRData data = new GetNovelByCategoryRData.fromJson(dataJson);
    return new GetNovelByCategoryResponse(
      baseResponse.error,
      baseResponse.message,
      baseResponse.stime,
      data,
    );
  }
}

class GetNovelByCategoryRData {
  final List<StoryMiniEntity> stories;
  const GetNovelByCategoryRData(this.stories);

  factory GetNovelByCategoryRData.fromJson(Map<String,dynamic> json) {
    List< dynamic> listRawGenre = json['stories'];
   // print("stories $listRawGenre");
    List<StoryMiniEntity> listGenre = listRawGenre.map((dynamic j) {
      return new StoryMiniEntity.fromJson(j);
    }).toList();
    
    return new GetNovelByCategoryRData(listGenre);
  }
}
