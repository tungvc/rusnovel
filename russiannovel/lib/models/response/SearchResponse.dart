import 'BaseResponse.dart';
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';

class SearchResponse extends BaseResponse {
  final SearchRData data;
  const SearchResponse(error, message, stime, {this.data})
      : super(error, message, stime);

  factory SearchResponse.fromJson(Map<String, dynamic> json) {
    BaseResponse baseResponse = new BaseResponse.fromJson(json);
    Map<String, dynamic> dataJson = BaseResponse.getDataField(json);
    SearchRData data = new SearchRData.fromJson(dataJson);
    return new SearchResponse(
      baseResponse.error,
      baseResponse.message,
      baseResponse.stime,
      data: data,
    );
  }
}
class SearchRData {
  final int idx;
  final List<StoryMiniEntity> stories;

  const SearchRData({this.idx, this.stories});

  factory SearchRData.fromJson(Map<String, dynamic> json) {
    List<dynamic> listRawStories = json['stories'];

    List<StoryMiniEntity> listStories =
        listRawStories.map((dynamic j) {
      return new StoryMiniEntity.fromJson(j);
    }).toList();

    return new SearchRData(
      idx: json['idx'],
      stories: listStories,
    );
  }
}