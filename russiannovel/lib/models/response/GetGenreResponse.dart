import 'BaseResponse.dart';

class GetGenreResponse extends BaseResponse {
  final GetGenreRData data;
  const GetGenreResponse(int error, String message, int stime, this.data)
      : super(error, message, stime);

  factory GetGenreResponse.fromJson(Map<String, dynamic> json) {
    BaseResponse baseResponse = new BaseResponse.fromJson(json);
    int errorCode = baseResponse.error;
    print('ErrorCode : $errorCode');
    Map<String, dynamic> dataJson = BaseResponse.getDataField(json);
    GetGenreRData data = new GetGenreRData.fromJson(dataJson);
    //print(" run to here");
    return new GetGenreResponse(
      baseResponse.error,
      baseResponse.message,
      baseResponse.stime,
      data,
    );
  }
}

class GetGenreRData {
  final List<GenreR> cates;
  const GetGenreRData(this.cates);

  factory GetGenreRData.fromJson(Map<String,dynamic> json) {
    
    List< dynamic > listRawGenre = json['cates'];
    List<GenreR> listGenre;
    listGenre = listRawGenre.map((dynamic j) {
      return new GenreR.fromJson(j);
    }).toList();
    return new GetGenreRData(listGenre);
  }
}

class GenreR {
  final String title; // de hien thi
  final String key; // dung de request

  const GenreR(this.title, this.key);

  factory GenreR.fromJson( Map<String,dynamic> json) {
    return new GenreR(
      
      json['title'],
      json['key'],
    );
  }
}
