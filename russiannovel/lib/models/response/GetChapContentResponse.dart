import 'BaseResponse.dart';

class GetChapContentResponse extends BaseResponse {
  final GetChapContentRData data;
  const GetChapContentResponse(int error, String message, int stime, this.data)
      : super(error, message, stime);

  factory GetChapContentResponse.fromJson(Map<String, dynamic> json) {
    BaseResponse baseResponse = new BaseResponse.fromJson(json);

    print('ErrorCode : ${baseResponse.error}');
    Map<String, dynamic> dataJson = BaseResponse.getDataField(json);
    GetChapContentRData data = new GetChapContentRData.fromJson(dataJson);
    return new GetChapContentResponse(
      baseResponse.error,
      baseResponse.message,
      baseResponse.stime,
      data,
    );
  }

}

class GetChapContentRData {
  final String content;
  const GetChapContentRData(this.content);

  factory GetChapContentRData.fromJson(Map<String, dynamic> json) {
    String content = json['content'];
    return new GetChapContentRData(content);
  }
}
