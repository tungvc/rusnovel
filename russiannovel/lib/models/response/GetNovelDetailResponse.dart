import 'BaseResponse.dart';
import '../entity/StoryEntity.dart';

class GetNovelDetailResponse extends BaseResponse {
  final GetNovelDetailRData data;
  const GetNovelDetailResponse(int error, String message, int stime, this.data)
      : super(error, message, stime);

  factory GetNovelDetailResponse.fromJson(Map<String, dynamic> json) {
    BaseResponse baseResponse = new BaseResponse.fromJson(json);
    int errorCode = baseResponse.error;
    print('ErrorCode : $errorCode');
    Map<String, dynamic> dataJson = BaseResponse.getDataField(json);
    GetNovelDetailRData data = new GetNovelDetailRData.fromJson(dataJson);
    return new GetNovelDetailResponse(
      baseResponse.error,
      baseResponse.message,
      baseResponse.stime,
      data,
    );
  }
}

class GetNovelDetailRData {
  final StoryEntity story;
  const GetNovelDetailRData(this.story);
  factory GetNovelDetailRData.fromJson(Map<String,dynamic> json) {
    StoryEntity entity = new StoryEntity.fromJson(json["story"]);
    return new GetNovelDetailRData(entity);
  }
}
