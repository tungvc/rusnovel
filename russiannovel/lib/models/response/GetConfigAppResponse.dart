import 'BaseResponse.dart';

class GetConfigAppResponse extends BaseResponse {
  final GetConfigAppRData data;

  const GetConfigAppResponse(int error, String message, int stime, this.data)
      : super(error, message, stime);

  factory GetConfigAppResponse.fromJson(Map<String, dynamic> json) {
    BaseResponse baseResponse = new BaseResponse.fromJson(json);

    print('ErrorCode : ${baseResponse.error}');
    Map<String, dynamic> dataJson = BaseResponse.getDataField(json);
    GetConfigAppRData data = new GetConfigAppRData.fromJson(dataJson);
    return new GetConfigAppResponse(
      baseResponse.error,
      baseResponse.message,
      baseResponse.stime,
      data,
    );
  }

}

class GetConfigAppRData {
  final bool lapp;
  final bool ldown;
  final int maxchapdown;
  final int minchapdown;
  final int readchapads;
  final int ratiodownall;

  const GetConfigAppRData(this.lapp, this.ldown, this.maxchapdown, this.minchapdown, this.readchapads, this.ratiodownall);

  factory GetConfigAppRData.fromJson(Map<String, dynamic> json) {
    bool content = json['lapp'];
    bool ldown = json['ldown'];
    int maxchapdown = json['maxchapdown'];
    int minchapdown = json['minchapdown'];
    int readchapads = json['readchapads'];
    int ratiodownall = json['ratiodownall'];
    return new GetConfigAppRData(content, ldown, maxchapdown, minchapdown, readchapads, ratiodownall);
  }
}
