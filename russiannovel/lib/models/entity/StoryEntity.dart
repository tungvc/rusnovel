import "./ChapEntity.dart";

class StoryEntity {
   String name;
   String title;
   String oTitle;
   String author;
   String urlCover;
   String description;
   String category;
   int ltUpdate;
   List<ChapEntity> listChaps;

   StoryEntity(
      {this.name,
      this.title,
      this.oTitle,
      this.author,
      this.urlCover,
      this.description,
      this.category,
      this.ltUpdate,
      this.listChaps});

   StoryEntity.initial()
      : name = "",
        title = "",
        oTitle = "",
        author = "",
        urlCover = "",
        description = "",
        category = "",
        ltUpdate = 0,
        listChaps = const [];

  factory StoryEntity.fromJson(Map<String, dynamic> json) {
    List<dynamic> listChapsRaw = json['listChaps'];
    List<ChapEntity> listChaps = listChapsRaw.map((j) {
      return new ChapEntity.fromJson(j);
    }).toList();
    return new StoryEntity(
      name: json["name"],
      title: json["title"],
      oTitle: json["otitle"],
      author: json["author"],
      urlCover: json["urlcover"],
      description: json["description"],
      category: json["category"],
      ltUpdate: json["ltupdate"],
      listChaps: listChaps,
    );
  }
}
