import 'package:russiannovel/models/response/GetGenreResponse.dart';

class GenreEntity {
  final String title; // de hien thi
  final String key; // dung de request

  const GenreEntity({this.title, this.key});

  factory GenreEntity.fromResponse(GenreR genreResponse) {
    return new GenreEntity(
      title: genreResponse.title,
      key: genreResponse.key,
    );
  }
}
