class ChapEntity implements Comparable<ChapEntity> {
   int chid;
   int chTime;
   String chName;

  int downloadStatus = 0; // 1 downloading 2 downloaded;
  
  ChapEntity({this.chid, this.chName, this.chTime});

  factory ChapEntity.fromJson(Map<String, dynamic> json) {
    return new ChapEntity(
        chid: json["chid"], chName: json["chname"], chTime: json["chtime"]);
  }

  @override
  int compareTo(ChapEntity other) => chid.compareTo(other.chid);
}
