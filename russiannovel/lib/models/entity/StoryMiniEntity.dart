class StoryMiniEntity implements Comparable<StoryMiniEntity> {
  final int cid;
  final String title;
  final String urlt;
  final String news;
  final int ltupdate;
  final String name;
  final String refurl;

  const StoryMiniEntity(
      {this.cid, this.title, this.urlt, this.news, this.ltupdate, this.name, this.refurl});

  factory StoryMiniEntity.fromJson(Map<String, dynamic> json) {
    return new StoryMiniEntity(
      cid: json['cid'],
      title: json["title"],
      urlt: json["urlt"],
      news: json["news"],
      ltupdate: json["ltupdate"],
      name: json['name'],
      refurl: json['refurl']
    );
  }

  Map<String,dynamic> toMap(){
    Map<String,dynamic> map = {
      "cid": cid,
      "title":title,
      "urlt":urlt,
      "news":news,
      "ltupdate":ltupdate,
      "name":name,
      "refurl":refurl
    };
    return map;
  }

  @override
  int compareTo(StoryMiniEntity other) => cid.compareTo(other.cid);
}
