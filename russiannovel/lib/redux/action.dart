
import 'package:russiannovel/models/entity/StoryEntity.dart';

abstract class BaseAction {
  const BaseAction();
}


class SelectStory extends BaseAction{
  final StoryEntity storyEntity;

  SelectStory(this.storyEntity);
}