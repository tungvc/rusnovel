import 'package:russiannovel/models/entity/ChapEntity.dart';
import 'package:russiannovel/models/entity/StoryEntity.dart';

class CurrentStoryState {
  final StoryEntity storyEntity;
  final ChapEntity chapEntity;

  CurrentStoryState({this.storyEntity, this.chapEntity});

  CurrentStoryState apply({StoryEntity sEnt, ChapEntity cEnt}) {
    return new CurrentStoryState(
      storyEntity: sEnt ?? this.storyEntity,
      chapEntity: cEnt ?? this.chapEntity,
    );
  }
}
