import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/flutter-search-bar/flutter_search_bar.dart';
import 'package:russiannovel/models/entity/GenreEntity.dart';
import 'package:russiannovel/models/enums/ScreenEnums.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'package:russiannovel/providers/StaticData.dart';
import 'package:russiannovel/utils/TextConstantUtils.dart';
import 'package:russiannovel/views/screens/CategoryPage.dart';
import 'package:russiannovel/views/screens/DownloadedPage.dart';
import 'package:russiannovel/views/screens/HomePage.dart';
import 'package:russiannovel/views/screens/HotStoryPage.dart';
import 'package:russiannovel/views/screens/RecentStoryPage.dart';
import 'package:russiannovel/views/screens/SearchPage.dart';
import 'package:russiannovel/views/screens/SettingPage.dart';
import 'package:russiannovel/views/screens/StoryCategoryPageNoAppBar.dart';
import 'package:russiannovel/views/screens/SubscribePage.dart';

class ScreenManager extends StatefulWidget {
  ScreenManager({Key key, this.title, this.useLightTheme, @required this.body})
      : super(key: key);

  final bool useLightTheme;
  final String title;
  final Widget body;

  final ScreenManagerState _screenManagerState = new ScreenManagerState();

  @override
  ScreenManagerState createState() => _screenManagerState;

  void changeScreen(ScreenEnums screen) {
    _screenManagerState.changeScreen(screen);
  }
}

class ScreenManagerState extends State<ScreenManager>
    with TickerProviderStateMixin {
  ScreenEnums _currentScreen = ScreenEnums.HomeScreen;
  String _currentTitle = "легкая новелла";
  SearchBar searchBar;
  AnimationController _controller;

  void changeScreen(ScreenEnums screen) {
    this.setState(() {
      this._currentScreen = screen;
      this._currentTitle = TextConstantUtils.getTitle(screen);
    });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    NovelFirebaseAnalytic.setCurrentScreen("Home", "HomePage");
//    this.loadData();
    new Timer.periodic(new Duration(milliseconds: 500), _incrementTimerCounter);
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
    );
  }

  void _incrementTimerCounter(Timer t) {
    if (StaticData.enableBackgroundDownload) {
      // process down load
      print("down chap");
      StaticData.downChap();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
//    SQLProvider.instance.close();
    _controller.dispose();
  }

  Widget getCurrentScreen(BuildContext context) {
    var content;
    switch (_currentScreen) {
      case ScreenEnums.RecentScreen:
        content = RecentStoryPage();
        break;
      case ScreenEnums.SubscribeScreen:
        content = SubscribePage();
        break;
      case ScreenEnums.HotScreen:
        content = HotStoryPage();
        break;
      case ScreenEnums.DownloadedScreen:
        content = DownloadedPage();
        break;
      default:
        content = HomePage(
          title: "Home Page",
        );
    }

    final Animation<Offset> position =
        new Tween(begin: const Offset(0.0, 1.0), end: Offset.zero).animate(
      new CurvedAnimation(parent: _controller, curve: Curves.decelerate),
    );

    return new Container(
      child: new Stack(
        children: <Widget>[
          content,
          new SlideTransition(
            position: position,
            child: new GestureDetector(
              onTap: () {
                _controller.reverse();
              },
              child: new Container(
                color: new Color.fromRGBO(0, 0, 0, 0.8),
              ),
            ),
          )
        ],
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(title: new Text(this._currentTitle), actions: [
      new IconButton(
          icon: new Icon(Icons.search),
          onPressed: () {
            _controller.reverse();
            Navigator.of(context)
                .push(new MaterialPageRoute(builder: (context) {
              return new SearchPage();
            }));
          }),
      new IconButton(
        icon: new Icon(Icons.history),
        onPressed: () {
          _controller.reverse();
          this.changeScreen(ScreenEnums.RecentScreen);
        },
      )
    ]);
  }

  void onSubmitted(String value) {
    this.setState(() {
//      changeScreen(ScreenEnums.HotScreen);
      Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
        return new SearchPage();
      }));
    });
  }

  ScreenManagerState() {
    searchBar = new SearchBar(
        colorBackButton: false,
        inBar: false,
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted);
  }

  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;
    int widthStory = 100;
    int countCol = (widthScreen ~/ widthStory).toInt();
    widthStory = (widthScreen ~/ countCol).toInt() - 6;

    return new Scaffold(
      appBar: searchBar.build(context),
      key: _scaffoldKey,
      body: getCurrentScreen(context),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: new Padding(
        padding: EdgeInsets.only(bottom: 50.0),
        child: floatActionButton(context),
      ),
    );
  }

  Widget floatActionButton(context) {
    final List<MenuPart> menuParts = [
      new MenuPart(
          icon: new Icon(Icons.home, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_HOME_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            changeScreen(ScreenEnums.HomeScreen);
          }),
      new MenuPart(
          icon: new Icon(Icons.star, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_SUBSCRIBE_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            changeScreen(ScreenEnums.SubscribeScreen);
          }),
      new MenuPart(
          icon: new Icon(Icons.arrow_downward, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_DOWNLOADED_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            changeScreen(ScreenEnums.DownloadedScreen);
          }),
      // new MenuPart(
      //     icon: new Icon(FontAwesomeIcons.tags, color: Colors.black87),
      //     label: new Text(AppConfig.langDefine.DRAWER_CATEGORY_LABEL_TEXT),
      //     onPress: () {
      //       _controller.reverse();
      //       Navigator.of(context).push(new MaterialPageRoute(
      //         builder: (context) {
      //           return new CategoryPage();
      //         },
      //       ));
      //     }),
      new MenuPart(
          icon: new Icon(Icons.settings, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_SETTING_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (context) {
                return new SettingPage();
              },
            ));
          }),
    ];

    Color backgroundColor = Colors.white;

    return new Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: new List.generate(menuParts.length, (int index) {
        Widget child = new Container(
          height: 60.0,
          width: 160.0,
          alignment: FractionalOffset.topRight,
          child: new ScaleTransition(
            scale: new CurvedAnimation(
              parent: _controller,
              curve: new Interval(0.0, 1.0 - index / menuParts.length / 2.0,
                  curve: Curves.bounceInOut),
            ),
            child: new GestureDetector(
              onTap: menuParts[index].onPress,
              child: new Container(
                decoration: new BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  borderRadius: new BorderRadius.all(new Radius.circular(50.0)),
                ),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    menuParts[index].label,
                    new FloatingActionButton(
                      heroTag: null,
                      backgroundColor: backgroundColor,
                      mini: true,
                      child: menuParts[index].icon,
                      onPressed: menuParts[index].onPress,
//                      onPressed: menuParts[index].onPress,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
        return child;
      }).toList()
        ..add(
          new FloatingActionButton(
            heroTag: null,
            child: new AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget child) {
                return new Transform(
                  transform:
                      new Matrix4.rotationZ(_controller.value * 0.5 * pi),
                  alignment: FractionalOffset.center,
                  child: new Icon(
                      _controller.isDismissed ? Icons.add : Icons.close),
                );
              },
            ),
            onPressed: () {
              if (_controller.isDismissed) {
                _controller.forward();
              } else {
                _controller.reverse();
              }
            },
          ),
        ),
    );
  }
}

class MenuPart {
  final Widget icon;
  final Widget label;
  final Function onPress;

  MenuPart({this.icon, this.label, this.onPress});
}
