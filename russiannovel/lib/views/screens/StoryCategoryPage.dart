import 'package:flutter/material.dart';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/models/entity/GenreEntity.dart';
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
import 'package:russiannovel/views/component/StoryCardRow.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'dart:async';

class StoryCategoryPage extends StatefulWidget {
  final GenreEntity genreEntity;
  StoryCategoryPage({Key key, this.genreEntity}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  StoryCategoryState createState() => new StoryCategoryState();
}

class StoryCategoryState extends State<StoryCategoryPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<StoryMiniEntity> listStoryMiniCategory = new List();
  final ScrollController _scrollController = new ScrollController();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    loadData();
    NovelFirebaseAnalytic.setCurrentScreen("StoryCategoryPage", "StoryCategoryPage");
  }

  void loadData() {
    NovelRequest.instance.getNovelByCategory(widget.genreEntity.key, 0, 100,
        (response) {
      setState(() {
        this.listStoryMiniCategory = response.data.stories;
        print(
            "load size storyminiInfo- category ${widget.genreEntity.title} : ${listStoryMiniCategory.length}");
      });
    }, (error) {
      // error
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(widget.genreEntity.title),
      ),
      body: new NotificationListener(
        onNotification: _onNotification,
        child: new RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: _onRefeshData,
          child: new ListView(
              padding: const EdgeInsets.all(1.0),
              controller: _scrollController,
              physics: const AlwaysScrollableScrollPhysics(),
              children:
                  listStoryMiniCategory.map((StoryMiniEntity storyMiniEntity) {
                return new StoryCardRow(
                    story: storyMiniEntity, scaffoldKey: _scaffoldKey);
              }).toList()),
        ),
      ),
      
    );
  }

  Future<Null>  _onRefeshData() {

    final Completer<Null> completer = new Completer<Null>();
    print("on RefeshData");    
    return completer.future.then((_) {});
  }

  bool _onNotification(ScrollNotification notification) {
    //print(notification);
    if (notification is OverscrollNotification) {
      if(_scrollController.offset > 0){
        print("load more");
        
      }
      
      //return true;
    }
    // if(notification is ScrollEndNotification){
    //   print(notification);
    // }

    // if (notification is ScrollUpdateNotification) {
      
    // }

    return true;
  }
}
