import 'dart:async';

import 'package:flutter/material.dart';
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
import 'package:russiannovel/providers/SQLDataAccess/SubscribeStoryDA.dart';
import 'package:russiannovel/views/component/LoadingIndicatorWidget.dart';
import 'package:russiannovel/views/component/StoryCardRow.dart';

class SubscribePage extends StatefulWidget {
  SubscribePage({
    Key key,
  }) : super(key: key);

  @override
  _SubscribePageState createState() => new _SubscribePageState();
}

class _SubscribePageState extends State<SubscribePage> {
  bool _isLoading = true;
  final _listStory = <StoryMiniEntity>[];
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      new GlobalKey<ScaffoldState>();
  final DismissDirection _dismissDirection = DismissDirection.startToEnd;

  void _getDataSuccess(List<StoryMiniEntity> list) {
    if (list != null) {
      this.setState(() {
        this._listStory.addAll(list);
        this._isLoading = false;
      });
    } else {
      print("get list fail");
    }
  }

  Future _load() async {
    SubscribeStoryDA.instance
        .selectAll()
        .then(_getDataSuccess)
        .catchError((err) {
      print("get subscribe story ex: $err");
    });
  }

  Widget _buildBody() {
    final emptyText = new Center(
        child: new Column(
      children: <Widget>[
        new Text(
          "o_O",
          style: Theme.of(context).textTheme.body1.copyWith(
                fontSize: 100.0,
              ),
        ),
        new Text("\nEmpty")
      ],
    ));

    final listView = new ListView(
      children: _listStory.map(_buildItem).toList(),
    );

    return new Container(
      child: _isLoading
          ? new LoadingIndicatorWidget()
          : _listStory.isEmpty ? emptyText : listView,
    );
  }

  Widget _buildItem(StoryMiniEntity item) {
    final ThemeData theme = Theme.of(context);
    return new Dismissible(
      key: new ObjectKey(item),
      direction: _dismissDirection,
      onDismissed: (DismissDirection direction) {
        setState(() {
          _listStory.remove(item);
          SubscribeStoryDA.instance.delete(item.cid);
        });
        _scaffoldKey.currentState.showSnackBar(
            new SnackBar(content: new Text('You deleted item ${item.title}')));
      },
      background: new Container(
          color: Colors.blue,
          child: const ListTile(
              leading:
                  const Icon(Icons.delete, color: Colors.white, size: 36.0))),
      child: new Container(
          decoration: new BoxDecoration(
              color: theme.canvasColor,
              border: new Border(
                  bottom: new BorderSide(color: theme.dividerColor))),
          child: new StoryCardRow(
            story: item,
          )),
    );
  }

  @override
  void initState() {
    super.initState();
    _load();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      key: _scaffoldKey,
      child: _buildBody(),
    );
  }
}
