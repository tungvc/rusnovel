//import 'dart:io';
//import 'package:russiannovel/models/enums/ScreenEnums.dart';
//import 'package:russiannovel/utils/TextConstantUtils.dart';
//import 'package:russiannovel/views/component/AboutLightNovelDialog.dart';
//import 'package:flutter/foundation.dart';
//import 'package:flutter/material.dart';
//import 'package:russiannovel/views/screens/ScreenManager.dart';
//import 'package:russiannovel/views/screens/SettingPage.dart';
//import 'package:russiannovel/views/screens/SubscribePage.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'CategoryPage.dart';
//
//class CustomDrawer extends StatelessWidget {
//  final ScreenManagerState screenManager;
//
//  const CustomDrawer({Key key, this.screenManager}) : super(key: key);
//
//  Widget buildLockAPP(BuildContext context) {
//    return new Drawer(
//      child: new ListView(children: <Widget>[
//        new UserAccountsDrawerHeader(
//          accountName: new Text(
//            "",
//            style: new TextStyle(
//                fontSize: 25.0,
//                color: Colors.black,
//                fontWeight: FontWeight.bold),
//          ),
//          accountEmail: new Text(""),
//          decoration: new BoxDecoration(
//            image: new DecorationImage(
//              fit: BoxFit.fill,
//              image: new AssetImage('assets/drawerheader.png'),
//            ),
//          ),
//        ),
//
//        new ListTile(
//            leading: new Icon(Icons.star),
//            title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_SUBSCRIBE_LABEL_TEXT)),
//            onTap: () {
//              Navigator.of(context).pop();
//              Navigator.of(context).push(new MaterialPageRoute(
//                builder: (context) {
//                  return new SubscribePage();
//                },
//              ));
//            }),
//
//        new Divider(),
//        new ListTile(
//            leading: new Icon(Icons.settings),
//            title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_SETTING_LABEL_TEXT)),
//            onTap: () {
//              Navigator.of(context).pop();
//              Navigator.of(context).push(new MaterialPageRoute(
//                builder: (context) {
//                  return new SettingPage();
//                },
//              ));
//            }),
//        new AboutLightNovel(),
//        new ListTile(
//            leading: new Icon(Icons.exit_to_app),
//            title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_EXIT_LABEL_TEXT)),
//            onTap: () {
//              exit(0);
//            })
//      ]),
//      elevation: 20.0,
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
////    if (AppConfig.lockAPP) {
////      return buildLockAPP(context);
////    }
//
//    return new Drawer(
//
//      child: new Container(
//        color: Theme.of(context).scaffoldBackgroundColor,
//        child: new ListView(
//            children: <Widget>[
//          new UserAccountsDrawerHeader(
//            accountName: new Text(
//              "",
//              style: new TextStyle(
//                  fontSize: 25.0,
//                  color: Colors.black,
//                  fontWeight: FontWeight.bold),
//            ),
//            accountEmail: new Text(""),
//            decoration: new BoxDecoration(
//              image: new DecorationImage(
//                fit: BoxFit.fill,
//                image: new AssetImage('assets/drawerheader.png'),
//              ),
//            ),
//          ),
//          new ListTile(
//              leading: new Icon(Icons.home),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_HOME_LABEL_TEXT)),
//              onTap: () {
//                Navigator.pop(context);
//                this.screenManager.changeScreen(ScreenEnums.HomeScreen);
//              }),
//          new ListTile(
//              leading: new Icon(Icons.poll),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_HOT_LABEL_TEXT)),
//              onTap: () {
//                Navigator.pop(context);
//                this.screenManager.changeScreen(ScreenEnums.HotScreen);
////              Navigator.of(context).push(new MaterialPageRoute(
////                builder: (context) {
////                  return new StoryCategoryPage(
////                      genreEntity: new GenreEntity(title: "Hot", key: "hot"));
////                },
////              ));
//              }),
//          new ListTile(
//              leading: new Icon(Icons.lightbulb_outline),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_SUGGEST_LABEL_TEXT)),
//              onTap: () {
//                Navigator.pop(context);
//                this.screenManager.changeScreen(ScreenEnums.SuggestScreen);
////              Navigator.of(context).push(new MaterialPageRoute(
////                builder: (context) {
////                  return new StoryCategoryPage(
////                      genreEntity:
////                          new GenreEntity(title: "Suggest", key: "suggest"));
////                },
////              ));
//              }),
//          new ListTile(
//              leading: new Icon(Icons.history),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_RECENT_LABEL_TEXT)),
//              onTap: () {
//                Navigator.of(context).pop();
//                this.screenManager.changeScreen(ScreenEnums.RecentScreen);
//              }),
//          new ListTile(
//              leading: new Icon(Icons.star),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_SUBSCRIBE_LABEL_TEXT)),
//              onTap: () {
//                Navigator.of(context).pop();
//                this.screenManager.changeScreen(ScreenEnums.SubscribeScreen);
////              Navigator.of(context).push(new MaterialPageRoute(
////                builder: (context) {
////                  return new SubscribePage();
////                },
////              ));
//              }),
//          new ListTile(
//              leading: new Icon(Icons.arrow_downward),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_DOWNLOADED_LABEL_TEXT)),
//              onTap: () {
//                Navigator.of(context).pop();
//                this.screenManager.changeScreen(ScreenEnums.DownloadedScreen);
////              Navigator.of(context).push(new MaterialPageRoute(
////                builder: (context) {
////                  return new DownloadedPage();
////                },
////              ));
//              }),
//          new ListTile(
//              leading: new Icon(FontAwesomeIcons.tags),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_CATEGORY_LABEL_TEXT)),
//              onTap: () {
//                Navigator.of(context).pop();
//                Navigator.of(context).push(new MaterialPageRoute(
//                  builder: (context) {
//                    return new CategoryPage();
//                  },
//                ));
//              }),
//          new Divider(),
//          new ListTile(
//              leading: new Icon(Icons.settings),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_SETTING_LABEL_TEXT)),
//              onTap: () {
//                Navigator.of(context).pop();
//                Navigator.of(context).push(new MaterialPageRoute(
//                  builder: (context) {
//                    return new SettingPage();
//                  },
//                ));
//              }),
//          new AboutLightNovel(),
//          new ListTile(
//              leading: new Icon(Icons.exit_to_app),
//              title: new Text(TextConstantUtils.getText(TextConstantEnums.DRAWER_EXIT_LABEL_TEXT)),
//              onTap: () {
//                exit(0);
//              })
//        ]),
//      ),
//      elevation: 20.0,
//    );
//  }
//}
