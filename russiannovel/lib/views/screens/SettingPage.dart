import 'package:flutter/material.dart';
import "package:russiannovel/app/LightNovelApp.dart";
import "package:russiannovel/config/AppConfig.dart";
import 'package:russiannovel/utils/ThemeUtils.dart';

final Map<double, String> textSizes = <double, String>{
  10.0: AppConfig.langDefine.SETTING_FONT_SIZE_SMALL_TEXT,
  15.0: AppConfig.langDefine.SETTING_FONT_SIZE_NORMAL_TEXT,
  20.0: AppConfig.langDefine.SETTING_FONT_SIZE_LARGE_TEXT,
  26.0: AppConfig.langDefine.SETTING_FONT_SIZE_HUGE_TEXT,
};

class SettingPage extends StatelessWidget {
  const SettingPage({Key key}) : super(key: key);

  Widget getThemeItem(ThemeEnums enu, ThemeEnums currentTheme) {
    return RadioListTile<ThemeEnums>(
      title:
          new Text(enu.toString().substring(enu.toString().indexOf('.') + 1)),
      value: enu,
      groupValue: currentTheme,
      onChanged: LightNovelApp.getInstance().changeTheme,
      selected: currentTheme == enu,
    );
  }

  @override
  Widget build(BuildContext context) {
    final ThemeEnums currentTheme = AppConfig.theme;
    final double textSize = AppConfig.fontSize;

    final List<Widget> themeItems = <Widget>[
      new Text(AppConfig.langDefine.SETTING_THEME_GROUP)
    ];
    ThemeEnums.values.forEach((k) {
      themeItems.add(getThemeItem(k, currentTheme));
    });

    final List<Widget> textSizeItems = <Widget>[
      new Text(AppConfig.langDefine.SETTING_FONT_SIZE_GROUP)
    ];

    textSizes.forEach((k, val) {
      textSizeItems.add(new RadioListTile<double>(
        secondary: const Icon(Icons.format_size),
        title: new Text(
          val,
          style: new TextStyle(fontSize: k),
        ),
        value: k,
        groupValue: textSize,
        selected: textSize == k,
        onChanged: LightNovelApp.getInstance().changeTextSize,
      ));
    });

    final List<Widget> allDrawItem = <Widget>[];
    allDrawItem.addAll(themeItems);
    allDrawItem.add(new Divider());
    allDrawItem.addAll(textSizeItems);
    allDrawItem.add(new Divider());

    return new Scaffold(
        appBar: new AppBar(
          title: new Text(AppConfig.langDefine.SETTING_TITLE),
        ),
        body: new ListView(
          primary: false,
          padding: const EdgeInsets.only(
              bottom: 10.0, left: 10.0, right: 10.0, top: 10.0),
          children: allDrawItem,
        ));
  }
}
