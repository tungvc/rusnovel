import 'package:flutter/material.dart';
import 'package:russiannovel/models/entity/GenreEntity.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/views/screens/StoryCategoryPage.dart';

class CategoryPage extends StatefulWidget {
  final List<GenreEntity> listGenre = new List();

  CategoryPage({Key key}) : super(key: key);

  @override
  CategoryPageState createState() => new CategoryPageState();
}

class CategoryPageState extends State<CategoryPage> {
  CategoryPageState();

  @override
  void initState() {
    super.initState();
    if (widget.listGenre.isEmpty) {
      NovelRequest.instance.getGenre((response) {
        setState(() {
          int length = response.data.cates.length;
          print("size categorys : $length");
          for (int i = 0; i < length; ++i) {
            widget.listGenre
                .add(new GenreEntity.fromResponse(response.data.cates[i]));
          }
        });
      }, (error) {
        // error
      });
    }
    NovelFirebaseAnalytic.setCurrentScreen("CategoryPage", "CategoryPage");
    //StaticData.init();
  }

  Iterable<Widget> get actorWidgets sync* {
    for (GenreEntity genre in widget.listGenre) {
      yield new Padding(
        padding: const EdgeInsets.all(4.0),
        child: new FilterChip(
          backgroundColor: new Color(0xFFFFCC80),
          label: new Text(genre.title),
          onSelected: (bool value) {
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (context) {
                return new StoryCategoryPage(genreEntity: genre);
              },
            ));
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
        appBar: new AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: new Text("Genres"),
        ),
        body: new SingleChildScrollView(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Wrap(
                children: actorWidgets.toList(),
              ),
              //new Text('Look for: ${_filters.join(', ')}'),
            ],
          ),
        ));
  }
}
