import 'package:flutter/material.dart';
import 'package:russiannovel/models/response/SearchResponse.dart';
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
import 'package:russiannovel/views/component/LoadingIndicatorWidget.dart';
import 'package:russiannovel/views/component/StoryCardRow.dart';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'package:loader_search_bar/loader_search_bar.dart';

import 'dart:async';

class SearchPage extends StatefulWidget {
  SearchPage({Key key}) : super(key: key);

  @override
  SearchPageState createState() => new SearchPageState();
}

class SearchPageState extends State<SearchPage> {
  String textSearch = "";
  SearchBar searchBar;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
  new GlobalKey<RefreshIndicatorState>();
  final ScrollController _scrollController = new ScrollController();

  bool _loadMore = false;
  int _nextSearchIndex = 0;
  bool _isFetching = false;
  bool _isError = false;
  int lasttimesearch = 0;

  List<StoryMiniEntity> _listStory = new List();

  Future<Null> _onRefresh() {
    _nextSearchIndex = 0;
    _listStory.clear();
    _loadMoreData();
    final Completer<Null> completer = new Completer<Null>();
    new Timer(const Duration(microseconds: 500), () {
      completer.complete(null);
    });
    return completer.future.then((_) {});
  }

  void _loadMoreData() {

    NovelRequest.instance.search(this.textSearch, _nextSearchIndex,
            (SearchResponse response) {
          _isFetching = false;
          _listStory.addAll(response.data.stories);
          _nextSearchIndex = response.data.idx + 1;
          this.setState(() {});
        }, (error) {
          // error
          print('show message error ${error.toString()}');
          this.setState(() {
            _isError = true;
          });
        });
  }

  void _search() {

    _nextSearchIndex =0;
    _listStory .clear();

    NovelRequest.instance.search(this.textSearch, _nextSearchIndex,
            (SearchResponse response) {
          _isFetching = false;

          _listStory.addAll(response.data.stories);
          _nextSearchIndex = response.data.idx + 1;
          this.setState(() {

          });
        }, (error) {
          // error
          print('show message error ${error.toString()}');

          _isError = true;
          this.setState(() {

          });

        });


  }

  @override
  void initState() {

    super.initState();
    _isFetching = false;


    //searchBar.beginSearch(context);

    // _search("a");
    NovelFirebaseAnalytic.setCurrentScreen("SearchPage", "SearchPage");
  }

  @override

  @override
  Widget build(BuildContext context) {
    Widget listResult;
    if (_isError) {
      listResult = new Center(
          child: new Column(
            children: <Widget>[
              new Text("=((",
                  style: new TextStyle(
                    fontSize: 100.0,
                  )),
              new Text("Oops, there are some error !",
                  style: new TextStyle(
                    fontSize: 20.0,
                  )),
              new RaisedButton(
                child: new Text("reload"),
                onPressed: () {
                  this.setState(() {
                    _isError = false;
                    _isFetching = true;
                    _loadMoreData();
                  });
                },
              )
            ],
          ));
    } else if (!_isFetching && !_loadMore && _listStory.length < 1) {
      listResult = new Center(
        child: new Text("Find nothing!"),
      );
    } else {
      listResult = _isFetching
          ? new LoadingIndicatorWidget()
          : new ListView(
          padding: const EdgeInsets.all(1.0),
          controller: _scrollController,
          physics: const AlwaysScrollableScrollPhysics(),
          children: _listStory.map((StoryMiniEntity storyMiniEntity) {
            return new StoryCardRow(
                story: storyMiniEntity, scaffoldKey: _scaffoldKey);
          }).toList());
    }

    return new Scaffold(
      appBar: SearchBar(
        iconified: false,
        defaultBar: AppBar(),
        onQueryChanged: (query) => _onQueryChanged(context, query),
        onQuerySubmitted: (query) => _onQuerySubmitted(context, query),
      ),
      body: new NotificationListener(
        onNotification: _onNotification,
        child: new RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: _onRefresh,
          child: listResult,
        ),
      ),
    );
  }

  _onQueryChanged(BuildContext context, String query) {
    //setState(() => _queryText = 'Query changed: $query');

    // if(DateTime.now().millisecondsSinceEpoch-lasttimesearch  > 500){
    //   this.textSearch = query;
    //   lasttimesearch = DateTime.now().millisecondsSinceEpoch;
    //   _search();
    // }


  }

  _onQuerySubmitted(BuildContext context, String query) {
    //setState(() => _queryText = 'Query submitted!');
    this.textSearch = query;
    _search();
  }


  // AppBar buildAppBar(BuildContext context) {
  //   return new AppBar(title: new Text('Search'), actions: [
  //     searchBar.getSearchAction(context),
  //   ]);
  // }

  bool _onNotification(ScrollNotification notification) {
    //print(notification);
    if (notification is OverscrollNotification) {
      if (_scrollController.offset > 0) {
        _loadMore = true;
      }
      return true;

      //return true;
    }
    if (notification is ScrollEndNotification) {
      if (_loadMore) {
        _loadMore = false;
        _loadMoreData();
      }
    }

    return true;
  }
}
