import 'dart:async';

import 'package:flutter/material.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/models/entity/ChapEntity.dart';
import "package:russiannovel/models/entity/StoryEntity.dart";
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
import 'package:russiannovel/models/response/GetChapContentResponse.dart';
import 'package:russiannovel/models/response/GetNovelDetailResponse.dart';
import 'package:russiannovel/providers/NovelFirebaseAdMob.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/providers/SQLDataAccess/RecentStoryDA.dart';
import 'package:russiannovel/providers/SQLDataAccess/RecentStoryData.dart';
import 'package:russiannovel/providers/SQLDataAccess/SubscribeStoryDA.dart';
import 'package:russiannovel/views/component/ChapRowWidget.dart';
import "package:russiannovel/views/component/StoryInfoWidget.dart";
import 'package:russiannovel/views/component/StoryReadTry.dart';
import 'package:russiannovel/views/screens/ChapReaderPage.dart';

enum DialogAction {
  cancel,
  discard,
  disagree,
  agree,
}

class StoryInfoPage extends StatefulWidget {
  final StoryMiniEntity storyMiniEntity;
  final imageCover;

  const StoryInfoPage({this.storyMiniEntity, this.imageCover});

  @override
  State<StatefulWidget> createState() => new _StoryInfoState();
}

class _StoryInfoState extends State<StoryInfoPage> with WidgetsBindingObserver {
  StoryEntity storyEntity;

  bool _isASC = true;
  String filterText = "";
  bool isSub = false;
  int _countDownloadChap = 0;
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      new GlobalKey<ScaffoldState>();
  final ScrollController _scrollController = new ScrollController();

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  // StoryInformationWidget storyInformationWidget;
  int _numChap = 10;
  bool _loadMore = false;

  @override
  void dispose() {
    // TODO: implement dispose
    NovelFirebaseAdMob.instance.removeBanner();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    NovelFirebaseAnalytic.setCurrentScreen("StoryInfoPage", "StoryInfoPage");

    storyEntity = new StoryEntity();
    storyEntity.title = this.widget.storyMiniEntity.title;
    storyEntity.name = this.widget.storyMiniEntity.name;
    storyEntity.urlCover = this.widget.storyMiniEntity.urlt;
    storyEntity.listChaps = new List();
    storyEntity.category = "";
    storyEntity.description = "";
    storyEntity.ltUpdate = this.widget.storyMiniEntity.ltupdate;

    NovelFirebaseAdMob.instance.loadBanner();

   // NovelRequest.instance.getNovelDetail(
   //     this.widget.storyMiniEntity.cid, _onFletchSuccess, _onFletchFail);
   _onRefreshData();
  }

  Future _onFletchSuccess(GetNovelDetailResponse response) async {

    isSub = await SubscribeStoryDA.instance
        .isSubscribed(this.widget.storyMiniEntity.cid);

    setState(() {
      this.storyEntity = response.data.story;
      _numChap = 10;
      this.filterText = "";
      RecentStoryDA.instance
          .selectByID(widget.storyMiniEntity.cid)
          .then((RecentStoryData entity) {
        if (entity != null) {
          String dialogContent = AppConfig.langDefine.STORY_INFO_CONTINUE_READ_DIALOG_TEXT;
          dialogContent = dialogContent.replaceAll(
              new RegExp('{{chaptitle}}'), entity.chapTitle);

          this.showDialogMessage<DialogAction>(
              context: context,
              child: new AlertDialog(
                  title: new Text(
                      '${AppConfig.langDefine.APP_NAME} : ${AppConfig.langDefine.ALERT_TITLE}'),
                  content: new Text(
                    dialogContent,
                    //style: dialogTextStyle
                  ),
                  actions: <Widget>[
                    new FlatButton(
                        child: new Text(AppConfig.langDefine
                                .STORY_INFO_DISAGREE_CONTINUE_READ_BUTTON_TEXT),
                        onPressed: () {
                          Navigator.pop(context, DialogAction.disagree);
                        }),
                    new FlatButton(
                        child: new Text(AppConfig.langDefine
                                .STORY_INFO_AGREE_CONTINUE_READ_BUTTON_TEXT),
                        onPressed: () {
                          Navigator.pop(context, DialogAction.agree);
                          for (int i = 0;
                              i < storyEntity.listChaps.length;
                              ++i) {
                            if (storyEntity.listChaps[i].chid ==
                                entity.chapID) {
                              showChapReaderPage(storyEntity.listChaps[i]);
                              break;
                            }
                          }
                        })
                  ]));
        }
      }).catchError((onError) => {});
    });
  }

  void _onFletchFail(dynamic err) {
    print("get data fail $err");
    // Scaffold.of(context).hideCurrentSnackBar(
    //   reason: SnackBarClosedReason.hide,
    // );
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text("Oops load data fail, Check your connection"),
    ));
  }

  void showDialogMessage<T>({BuildContext context, Widget child}) {
    showDialog<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {
      // The value passed to Navigator.pop() or null.
      // if (value != null) {
      //   _scaffoldKey.currentState.showSnackBar(
      //       new SnackBar(content: new Text('You selected: $value')));
      // }
    });
  }

  Future _onSubscribeClick() async {
    if (isSub) {
      print("unsubscribe");
      SubscribeStoryDA.instance.delete(this.widget.storyMiniEntity.cid);
    } else {
      print("subscribe");
      await SubscribeStoryDA.instance.insert(new StoryMiniEntity(
        cid: this.widget.storyMiniEntity.cid,
        ltupdate: storyEntity.ltUpdate,
        name: storyEntity.name,
        news: storyEntity.listChaps
            .elementAt(storyEntity.listChaps.length - 1)
            .chName,
        title: storyEntity.title,
        urlt: storyEntity.urlCover,
      ));
    }
    this.setState(() {
      this.isSub = !isSub;
    });
  }

  Future<Null> _onRefreshData() {
     NovelRequest.instance.getNovelDetail(
        this.widget.storyMiniEntity.cid, _onFletchSuccess, _onFletchFail);
    final Completer<Null> completer = new Completer<Null>();
    new Timer(const Duration(microseconds: 500), () {
      completer.complete(null);
    });
    return completer.future.then((_) {});
  }

  @override
  Widget build(BuildContext context) {
    final Widget unsubscribeButton = new IconButton(
      icon: new Icon(Icons.star_border),
      onPressed: _onSubscribeClick,
      tooltip: "Subcribe this story",
    );
    final Widget subscribedButton = new IconButton(
      icon: new Icon(
        Icons.star,
        color: Colors.yellow,
      ),
      onPressed: _onSubscribeClick,
      tooltip: "unsubcribe this story",
    );

    final Widget buttonSort = new IconButton(
        icon: new Icon(Icons.sort_by_alpha),
        onPressed: () {
          this.setState(() {
            _isASC = !_isASC;
          });
        });

    final Widget searchBar = new Padding(
      child: new Row(
        children: <Widget>[
          new Flexible(
            child: new TextField(
              decoration: new InputDecoration(
                  border: null,
                  hintText: AppConfig.langDefine.STORY_INFO_FILTER_HINT_TEXT),
              onSubmitted: (text) {
                this.setState(() {
                  filterText = text;
                });
              },
              onChanged: (text) {
                this.setState(() {
                  filterText = text;
                });
              },
            ),
          ),
          buttonSort,
        ],
      ),
      padding: new EdgeInsets.only(left: 20.0, right: 10.0),
    );

    List<ChapEntity> _chapListClone =
        new List<ChapEntity>.from(storyEntity.listChaps.where((chap) {
      if (filterText.isEmpty || (chap.chName.toString()).toLowerCase().contains(filterText.toLowerCase())) {
        return true;
      }
      return false;

    }));
    if (_numChap > _chapListClone.length) _numChap = _chapListClone.length;
    if (!_isASC) {
      _chapListClone = new List<ChapEntity>.from(_chapListClone.reversed)
          .sublist(0, _numChap);
    } else {
      _chapListClone =
          new List<ChapEntity>.from(_chapListClone).sublist(0, _numChap);
    }

    Widget listChap = new Column(
      //mainAxisAlignment: MainAxisAlignment.start,
      //crossAxisAlignment: CrossAxisAlignment.start,
      //mainAxisSize: MainAxisSize.max,
      //mainAxisAlignment: MainAxisAlignment.center,
      //crossAxisAlignment: CrossAxisAlignment.start,
      children: _chapListClone.map((ChapEntity chapInfo) {
        return new ChapRowWidget(
            storyID: widget.storyMiniEntity.cid,
            entity: chapInfo,
            onPressed: () {
              print("tab on chap");
              this.setState(() {
                print("save recent story");
                RecentStoryDA.instance.saveRecentStory(
                    widget.storyMiniEntity.cid,
                    storyEntity.title,
                    storyEntity.urlCover,
                    chapInfo.chid,
                    chapInfo.chName);
              });
              //_scaffoldKey.currentState.showSnackBar(new SnackBar(
              //    content: new Text('loading ${chapInfo.chName}')));
              showChapReaderPage(chapInfo);
            },
            onDownloadChap: () {
              print("tab on download chap $_countDownloadChap ");
              _countDownloadChap++;
              Function downAChap = () {
                // down chap
                _countDownloadChap = 0;
                setState(() {
                  chapInfo.downloadStatus = 1;
                });
                NovelRequest.instance
                    .getChapContent(widget.storyMiniEntity.cid, chapInfo.chid,
                        (GetChapContentResponse response) {
                  print("download ${chapInfo.chid} start");
                  //update to gui
                  setState(() {
                    chapInfo.downloadStatus = 2;
                  });
                }, (error) {});
                NovelFirebaseAdMob.instance.removeBanner();
              };

              NovelFirebaseAdMob.instance.fnListenerClientEvent = downAChap;
              NovelFirebaseAdMob.instance.fnListenerFailedToShow = downAChap;
              NovelFirebaseAdMob.instance.fnListenerFailedToLoad = downAChap;

              if (_countDownloadChap >= AppConfig.maxChapDown) {
                // show dialog click to download

                //show ads ra
                NovelFirebaseAdMob.instance.showBanner();
                this.showDialogMessage<DialogAction>(
                    context: context,
                    child: new AlertDialog(
                        title: const Text('Light Novel : Alert'),
                        content: new Text(
                          "You limited for download. Please click ads to continue!!.  \nThankui for support LightNovel!",
                          //style: dialogTextStyle
                        ),
                        actions: <Widget>[
                          new FlatButton(

                              child: new Text(AppConfig.langDefine
                                      .STORY_INFO_DISAGREE_CONTINUE_READ_BUTTON_TEXT),
                              onPressed: () {
                                Navigator.pop(context, DialogAction.disagree);
                              }),
                          new FlatButton(
                              child: new Text(AppConfig.langDefine
                                      .STORY_INFO_AGREE_CONTINUE_READ_BUTTON_TEXT),
                              onPressed: () async {
                                Navigator.pop(context, DialogAction.agree);
                                //NovelFirebaseAdMob.instance.showInterstitial();
                              })
                        ]));
              }

              //_scaffoldKey.currentState.showSnackBar(
              //  new SnackBar(content: new Text('Đang tải chap')));
              setState(() {
                chapInfo.downloadStatus = 1;
              });
              NovelRequest.instance
                  .getChapContent(widget.storyMiniEntity.cid, chapInfo.chid,
                      (GetChapContentResponse response) {
                print("download ${chapInfo.chid} start");
                //update to gui
                setState(() {
                  chapInfo.downloadStatus = 2;
                });
              }, (error) {});

              if (_countDownloadChap == 1) {
                NovelFirebaseAdMob.instance.loadBanner();
              }

              if (_countDownloadChap > AppConfig.minChapDown) {
                // show ads ra
                NovelFirebaseAdMob.instance.showBanner();
              }
            });
      }).toList(),
    );

    Widget readTryWidget;

    StoryInformationWidget storyInformationWidget = new StoryInformationWidget(
        storyId: widget.storyMiniEntity.cid, storyEntity: storyEntity, imageCover: widget.imageCover,);

    Widget bodyWidget = new NotificationListener(
        onNotification: _onNotification,
        child: new RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _onRefreshData,
            child: new SingleChildScrollView(
              controller: _scrollController,
              physics: const AlwaysScrollableScrollPhysics(),
              child: new Container(
                child: new Column(
                  children: <Widget>[
                    storyInformationWidget,
                    new Divider(
                      indent: 1.0,
                      height: 1.0,
                      color: Colors.grey[600],
                    ),
                    searchBar,
                    listChap
                  ],
                ),
              ),
            )));
    if (AppConfig.lockAPP) {
      readTryWidget = new StoryReadTry(
          storyID: widget.storyMiniEntity.cid,
          chapID: storyEntity.listChaps[storyEntity.listChaps.length - 1].chid);
      //body widget

      bodyWidget = new NotificationListener(
          onNotification: _onNotification,
          child: new SingleChildScrollView(
            controller: _scrollController,
            physics: const AlwaysScrollableScrollPhysics(),
            child: new Container(
              child: new Column(
                children: <Widget>[
                  storyInformationWidget,
                  new Divider(
                    indent: 1.0,
                    height: 1.0,
                    color: Colors.grey[600],
                  ),
                  readTryWidget,
                  //searchBar,
                  //listChap
                ],
              ),
            ),
          ));
    }

    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: new Text(storyEntity.title),
          actions: <Widget>[
            isSub ? subscribedButton : unsubscribeButton,
          ],
        ),
        body: bodyWidget);
  }

  bool _onNotification(ScrollNotification notification) {
    // print(notification);
    // if (notification is OverscrollNotification) {
    //   if (_scrollController.offset. > 0) {
    //     if (_numChap != storyEntity.listChaps.length) _loadMore = true;
    //   }
    //   return true;

    //   //return true;
    // }
    if (notification is ScrollEndNotification) {
      if (_numChap != storyEntity.listChaps.length) _loadMore = true;
      if (_loadMore) {
        _loadMore = false;
        if (_numChap < storyEntity.listChaps.length - 50) {
          this._numChap += 50;
        } else {
          this._numChap = storyEntity.listChaps.length;
        }
        this.setState(() {});
      }
    }

    return true;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('change lifecycle $state');
    setState(() {
      switch (state.index) {
        case 0: // resumed

          break;
        case 1: // inactive
          //this.initState();
          //this.storyInformationWidget.createState();
          break;
        case 2: // paused

          break;
      }
    });
  }

  void showChapReaderPage(ChapEntity chapEntity) {
    Navigator.of(context).push(new MaterialPageRoute(
        //fullscreenDialog: true,

        builder: (context) {
      return new ChapReaderPage(
        storyId: widget.storyMiniEntity.cid,
        listChaps: storyEntity.listChaps,
        currentChap: chapEntity,
        onChapChanged: (chapChanged) {
          this.setState(() {
            print("save recent story");
            RecentStoryDA.instance.saveRecentStory(
                widget.storyMiniEntity.cid,
                storyEntity.title,
                storyEntity.urlCover,
                chapChanged.chid,
                chapChanged.chName);
          });
        },
      );
    }));
  }
}
