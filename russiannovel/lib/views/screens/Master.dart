//import 'dart:async';
//import 'dart:io';
//
//import 'package:flutter/material.dart';
////import "package:flutter_search_bar/flutter_search_bar.dart";
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:russiannovel/config/AppConfig.dart';
//import 'package:russiannovel/models/entity/GenreEntity.dart';
//import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
//import 'package:russiannovel/models/enums/ScreenEnums.dart';
//import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
//import 'package:russiannovel/providers/NovelRequest.dart';
//import 'package:russiannovel/providers/SQLProvider.dart';
//import 'package:russiannovel/providers/StaticData.dart';
//import 'package:russiannovel/views/component/StoryCardColumn.dart';
//import 'package:russiannovel/views/screens/CategoryPage.dart';
//import 'package:russiannovel/views/screens/Drawer.dart';
//import 'package:russiannovel/views/screens/HomePage.dart';
//import 'package:russiannovel/views/screens/RecentStoryPage.dart';
//import 'package:russiannovel/views/screens/SearchPage.dart';
//import 'package:russiannovel/views/screens/SettingPage.dart';
//import 'package:russiannovel/views/screens/StoryCategoryPage.dart';
//import 'package:russiannovel/views/screens/SubscribePage.dart';
//
//class Master extends StatefulWidget {
//  Master({Key key, this.title, this.useLightTheme, @required this.body})
//      : super(key: key);
//
//  final bool useLightTheme;
//  final String title;
//  final Widget body;
//
//  @override
//  MasterState createState() => new MasterState();
//}
//
//class MasterState extends State<Master> {
//  ScreenEnums _currentScreen = ScreenEnums.HomeScreen;
//  String _currentTitle = "Light Novel";
//  SearchBar searchBar;
//
////  final _maxGetNovel = 50;
////  int _idxGetNovel = 0;
//
////  bool _loadMore = false;
//
//  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//
//  @override
//  void initState() {
//    super.initState();
//
//    print("re init");
//    NovelFirebaseAnalytic.setCurrentScreen("Home", "HomePage");
////    this.loadData();
//    new Timer.periodic(new Duration(milliseconds: 500), _incrementTimerCounter);
//  }
//
//  void _incrementTimerCounter(Timer t) {
//    if (StaticData.enableDownload) {
//      // process down load
//      print("down chap");
//      StaticData.downChap();
//    }
//  }
//
//  @override
//  void dispose() {
//    // TODO: implement dispose
//    super.dispose();
//    print("dispose");
////    SQLProvider.instance.close();
//  }
//
////  void loadData() {
////    String category = "update";
////    if (AppConfig.lockAPP) {
////      category = "hot";
////    }
////
////    NovelRequest.instance.getNovelByCategory(category, idxGetNovel, maxGetNovel,
////        (response) {
////      setState(() {
////        StaticData.listStoryMiniInfoUpdate.addAll(response.data.stories);
////        idxGetNovel = maxGetNovel + idxGetNovel;
////        this.setState(() {});
////        _loadMore = false;
////        print("load size storyminiInfo-update : ${StaticData
////                .listStoryMiniInfoUpdate.length}");
////      });
////    }, (error) {
////      // error
////      _loadMore = false;
////    });
////  }
//
//  Widget getCurrentScreen(BuildContext context) {
//    switch (_currentScreen) {
//      case ScreenEnums.HotScreen:
//        return new StoryCategoryPage(
//            genreEntity: new GenreEntity(title: "Hot", key: "hot"));
//      case ScreenEnums.RecentScreen:
//        return new RecentStoryPage();
//      case ScreenEnums.SubscribeScreen:
//        return new SubscribePage();
//      default:
//        return new HomePage(
//          title: "Home Page",
//        );
//    }
//  }
//
//  AppBar buildAppBar(BuildContext context) {
//    return new AppBar(title: new Text(widget.title), actions: [
//      searchBar.getSearchAction(context),
////      new IconButton(
////        icon: new Icon(Icons.history),
////        onPressed: () {
////          Navigator.of(context).push(new MaterialPageRoute(
////            builder: (context) {
////              return new RecentStoryPage();
////            },
////          ));
////        },
////      )
//    ]);
//  }
//
//  void onSubmitted(String value) {
//    setState(() {
//      Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
//        return new SearchPage(
//          searchText: value,
//        );
//      }));
//    });
//  }
//
//  MasterState() {
//    searchBar = new SearchBar(
//        colorBackButton: false,
//        inBar: false,
//        buildDefaultAppBar: buildAppBar,
//        setState: setState,
//        onSubmitted: onSubmitted);
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    double widthScreen = MediaQuery.of(context).size.width;
//    int widthStory = 100;
//    int countCol = (widthScreen ~/ widthStory).toInt();
//    widthStory = (widthScreen ~/ countCol).toInt() - 6;
//
//    final getBottomAppBar = new BottomAppBar(
//        child: new Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            mainAxisSize: MainAxisSize.max,
//            children: <Widget>[
//          new IconButton(
//              icon: new Icon(Icons.home),
//              tooltip: "Home",
//              onPressed: () => this.setState(() {
//                    _currentScreen = ScreenEnums.HomeScreen;
//                    _currentTitle = "Home Page";
//                  })),
//          new IconButton(
//              icon: new Icon(Icons.poll),
//              tooltip: "HOT",
//              onPressed: () => this.setState(() {
//                    _currentScreen = ScreenEnums.HotScreen;
//                    _currentTitle = "Hot Story";
//                  })),
//          new IconButton(
//              icon: new Icon(Icons.history),
//              tooltip: "Recent",
//              onPressed: () => this.setState(() {
//                    _currentScreen = ScreenEnums.RecentScreen;
//                    _currentTitle = "Recent story";
//                  })),
//          new IconButton(
//              icon: new Icon(Icons.star),
//              tooltip: "Subscribe",
//              onPressed: () => this.setState(() {
//                    _currentScreen = ScreenEnums.SubscribeScreen;
//                    _currentTitle = "Subscribed";
//                  })),
//          new IconButton(
//              icon: new Icon(Icons.more_vert),
//              tooltip: "More",
//              onPressed: () => showModalBottomSheet(
//                  context: context,
//                  builder: (context) => Drawer(
//                        child: new Column(
//                          children: <Widget>[
//                            new ListTile(
//                                leading: new Icon(FontAwesomeIcons.tags),
//                                title: new Text('Category'),
//                                onTap: () {
//                                  Navigator.of(context).pop();
//                                  Navigator
//                                      .of(context)
//                                      .push(new MaterialPageRoute(
//                                    builder: (context) {
//                                      return new CategoryPage();
//                                    },
//                                  ));
//                                }),
//                            new ListTile(
//                                leading: new Icon(Icons.settings),
//                                title: new Text('Setting'),
//                                onTap: () {
//                                  Navigator.of(context).pop();
//                                  Navigator
//                                      .of(context)
//                                      .push(new MaterialPageRoute(
//                                    builder: (context) {
//                                      return new SettingPage();
//                                    },
//                                  ));
//                                }),
//                            //new AboutLightNovel(),
//                            new ListTile(
//                                leading: new Icon(Icons.exit_to_app),
//                                title: new Text('Exit'),
//                                onTap: () {
//                                  exit(0);
//                                })
//                          ],
//                        ),
//                      ))),
//        ]));
//
//    return new Scaffold(
////      drawer: new CustomDrawer(),
//      appBar: searchBar.build(context),
//      key: _scaffoldKey,
//      body: getCurrentScreen(context),
//      bottomNavigationBar: getBottomAppBar,
//
//    );
//  }
//}
