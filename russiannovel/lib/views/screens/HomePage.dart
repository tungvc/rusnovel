import 'dart:async';

import 'package:flutter/material.dart';
import 'package:russiannovel/config/AppConfig.dart';
import "package:russiannovel/flutter-search-bar/flutter_search_bar.dart";
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
import 'package:russiannovel/providers/NovelFirebaseAdMob.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/providers/StaticData.dart';
import 'package:russiannovel/views/component/StoryCardColumn.dart';
import 'package:russiannovel/views/screens/RecentStoryPage.dart';
import 'package:russiannovel/views/screens/SearchPage.dart';

class HomePage extends StatefulWidget {
  HomePage({
    Key key,
    this.title,
    this.useLightTheme,
  }) : super(key: key);

  final bool useLightTheme;
  final String title;

  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {
  SearchBar searchBar;

  // bool isFletching = false;
  int idxGetNovel = 0;
  final maxGetNovel = 50;
  bool _loadMore = false;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  final ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    NovelRequest.instance.getConfigApp((response) {
      setState(() {
        AppConfig.lockAPP = response.data.lapp;
        AppConfig.lockDOWN = response.data.ldown;
        AppConfig.maxChapDown = response.data.maxchapdown;
        AppConfig.minChapDown = response.data.minchapdown;
        AppConfig.ratioDownAll = response.data.ratiodownall;
        AppConfig.readChapAds = response.data.readchapads;
      });
    }, (error) {});
    NovelFirebaseAnalytic.setCurrentScreen("Home", "HomePage");
    NovelFirebaseAdMob.instance.init();
    NovelFirebaseAdMob.instance.loadBanner();
    this.loadData();
    new Timer.periodic(new Duration(milliseconds: 500), _incrementTimerCounter);
  }

  void _incrementTimerCounter(Timer t) {
    if (StaticData.enableBackgroundDownload) {
      // process down load
      print("down chap");
      StaticData.downChap();
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
//    SQLProvider.instance.close();
  }

  void loadData() {
    String category = "update";
    if (AppConfig.lockAPP) {
      category = "hot";
    }

    NovelRequest.instance.getNovelByCategory(category, idxGetNovel, maxGetNovel,
        (response) {
      setState(() {
        StaticData.listStoryMiniInfoUpdate.addAll(response.data.stories);
        idxGetNovel = maxGetNovel + idxGetNovel;
        this.setState(() {});
        _loadMore = false;
        print(
            "load size storyminiInfo-update : ${StaticData.listStoryMiniInfoUpdate.length}");
      });
    }, (error) {
      // error
      _loadMore = false;
    });
  }

  void refesh() {
    String category = "update";
    if (AppConfig.lockAPP) {
      category = "hot";
    }
    this.idxGetNovel = 0;
    NovelRequest.instance.getNovelByCategory(category, idxGetNovel, maxGetNovel,
        (response) {
      setState(() {
        StaticData.listStoryMiniInfoUpdate = response.data.stories;
        idxGetNovel = maxGetNovel + idxGetNovel;
        this.setState(() {});
        _loadMore = false;
        print(
            "refesh size storyminiInfo-update : ${StaticData.listStoryMiniInfoUpdate.length}");
      });
    }, (error) {
      // error
      _loadMore = false;
    });
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(title: new Text(widget.title), actions: [
      searchBar.getSearchAction(context),
      new IconButton(
        icon: new Icon(Icons.history),
        onPressed: () {
          Navigator.of(context).push(new MaterialPageRoute(
            builder: (context) {
              return new RecentStoryPage();
            },
          ));
        },
      )
    ]);
  }

  void onSubmitted(String value) {
    setState(() {
      Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
        return new SearchPage(
        );
      }));
    });
  }

  HomePageState() {
    searchBar = new SearchBar(
        colorBackButton: false,
        inBar: false,
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted);
  }

  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;
    double widthStory = AppConfig.defaultImageWidth;
    int countCol = (widthScreen ~/ widthStory);
    widthStory = (widthScreen ~/ countCol) - 6.0;
    return new Container(
      key: _scaffoldKey,
      child: new NotificationListener(
        onNotification: _onNotification,
        child: new RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: _onRefreshData,
          child: new GridView.count(
              crossAxisCount: countCol,
              childAspectRatio: 0.63,
              padding: const EdgeInsets.all(3.0),
              mainAxisSpacing: 5.0,
              crossAxisSpacing: 5.0,
              controller: _scrollController,
              physics: const AlwaysScrollableScrollPhysics(),
              children: StaticData.listStoryMiniInfoUpdate
                  .map((StoryMiniEntity entity) {
                return new StoryCardColumn(
                  story: entity,
                  scaffoldKey: _scaffoldKey,
                  widthStoryCard: widthStory,
                );
              }).toList()),
        ),
      ),
    );
  }

  Future<Null> _onRefreshData() {
    refesh();
    final Completer<Null> completer = new Completer<Null>();
    new Timer(const Duration(microseconds: 500), () {
      completer.complete(null);
    });
    return completer.future.then((_) {});
  }

  bool _onNotification(ScrollNotification notification) {
    //print(notification);
    if (notification is OverscrollNotification) {
      if (_loadMore == false && _scrollController.offset > 0) {
        _loadMore = true;
      }
      return true;

      //return true;
    }
    if (notification is ScrollEndNotification) {
      if (_loadMore) {
        loadData();
      }
      return true;
    }

    return true;
  }
}
