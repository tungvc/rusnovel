import 'dart:async';

import 'package:flutter/material.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/models/entity/ChapEntity.dart';
import 'package:russiannovel/models/response/GetChapContentResponse.dart';
import 'package:russiannovel/providers/NovelFirebaseAdMob.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/utils/StringUtils.dart';
import 'package:russiannovel/views/component/LoadingIndicatorWidget.dart';
import 'package:russiannovel/views/screens/SettingPage.dart';
import 'package:flutter_html_view/flutter_html_view.dart';

final Map<double, String> textSizesHtml = <double, String>{
  10.0: 'h5',
  15.0: 'h4',
  20.0: 'h3',
  26.0: 'h1',
};

enum DialogAction { cancel, ok }

class ChapReaderPage extends StatefulWidget {
  ChapReaderPage(
      {Key key,
      this.storyId,
      this.listChaps,
      this.currentChap,
      this.onChapChanged})
      : super(key: key);

  final List<ChapEntity> listChaps;
  final ChapEntity currentChap;
  final Function onChapChanged;
  final int storyId;

  @override
  _ChapReaderState createState() =>
      new _ChapReaderState(currentIndex: listChaps.indexOf(currentChap));
}

class _ChapReaderState extends State<ChapReaderPage> {
  //fake data
  String currentContent = "No Content";
  int currentIndex;
  ChapEntity currentChap;
  bool _isFetching = false;
  int _countChapRead = 0;
  String prepareContent = "Prepare content";
  bool changeContent = false;
  int delta = 0;
  String messageError = "";

  //theme
  // Color _textColor;
  // Color _backgroundColor;

  DismissDirection _dismissDirection = DismissDirection.horizontal;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  final ScrollController _scrollController = new ScrollController();

  _ChapReaderState({this.currentIndex});

  @override
  void initState() {
    super.initState();
    currentChap = widget.listChaps.elementAt(currentIndex);
    _loadContent();

    if (currentIndex == 0) {
      this.setState(() {
        this._dismissDirection = DismissDirection.startToEnd;
      });
    }
    if (currentIndex == widget.listChaps.length - 1) {
      this.setState(() {
        this._dismissDirection = DismissDirection.endToStart;
      });
    }

    NovelFirebaseAnalytic.setCurrentScreen(
        "ChapReaderPage", "ChapReaderPage" + currentChap.chName);

    NovelFirebaseAdMob.instance.fnListenerClientEvent = clickAds;
    NovelFirebaseAdMob.instance.fnListenerFailedToLoad = loadAdsFailEvent;
    NovelFirebaseAdMob.instance.fnListenerFailedToShow = loadAdsFailEvent;
    NovelFirebaseAdMob.instance.loadBanner();
  }

  void _loadContent() {
    this.setState(() {
      _isFetching = true;
    });

    NovelRequest.instance.getChapContent(widget.storyId, currentChap.chid,
        this._onFetchSuccess, this._onFetchError);
  }

  void _onFetchSuccess(GetChapContentResponse response) {
    this.setState(() {
      _isFetching = false;

      if (this.changeContent) {
        this.prepareContent = StringUtils.html2str(response.data.content);
      } else {
        this.currentContent = StringUtils.html2str(response.data.content);
      }
      //_scrollController.jumpTo(0.0);
    });
  }

  String _changeSizeFont(String content) {
    String htmlsize = textSizesHtml[AppConfig.fontSize].toString();

    return StringUtils.html2strWithHtmlSize(content, htmlsize);
  }

  void _onFetchError(dynamic error) {
    print(error);
    this.setState(() {
      _isFetching = false;
      this.currentContent = "Truyện Đang Có Lỗi ! Vui Lòng Thử Lại Sau";
      this.prepareContent = "Truyện Đang Có Lỗi ! Vui Lòng Thử Lại Sau";
    });
  }

  void clickAds() {
    print("clickAds");
  }

  void loadAdsFailEvent() {
    print("loadAdsFaidEvent");
  }

  Widget buildItem(String chapContent) {
    if (chapContent.length < 128) {
      // auto send chap loi
      String message =
          "{\"chapid\":${this.widget.currentChap.chid}, \"storyid\":${this.widget.storyId}, \"message\":\"${this.messageError}\"}";

      NovelRequest.instance.reportError(message);

      return new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Center(
              child: new Text(
                ":(",
                style: Theme.of(context).textTheme.body1.copyWith(
                      fontSize: 100.0,
//                      color: new Color(AppConfig.textChapReaderColor),
                    ),
              ),
            ),
            new Text(
              "\nOop",
              style: new TextStyle(
//                color: new Color(AppConfig.textChapReaderColor),
                  ),
            )
          ]);
    }
    final Widget bgPrevious = new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Row(
//            mainAxisAlignment: ,
            children: <Widget>[
              Icon(
                Icons.navigate_before,
                size: 50.0,
              ),
              new DefaultTextStyle(
                style: Theme.of(context).textTheme.body1.copyWith(
//                  fontFamily: AppConfig.fontFamily,
                      fontSize: AppConfig.fontSize,
                      //color: _textColor,
                    ),
                child: new Text(AppConfig.langDefine.CHAP_READER_PREVIOUS_CHAP),
              ),
            ],
          )
        ],
      ),
    );
    final Widget bgNext = new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              new DefaultTextStyle(
                style: Theme.of(context).textTheme.body1.copyWith(
//                  fontFamily: AppConfig.fontFamily,
                      fontSize: AppConfig.fontSize,
                    ),
                child: new Text(AppConfig.langDefine.CHAP_READER_NEXT_CHAP),
              ),
              Icon(
                Icons.navigate_next,
                //color: _textColor,
                size: 50.0,
              ),
            ],
          ),
        ],
      ),
    );

    return new Dismissible(
        key: new Key(chapContent),
        onDismissed: (direction) {
          this.setState(() {
            changeContent = !changeContent;
          });

          if (direction == DismissDirection.endToStart) {
            //do previous
            this.previousChap();
          } else if (direction == DismissDirection.startToEnd) {
            //do next
            this.nextChap();
          }
        },
        direction: _dismissDirection,
        background: bgPrevious,
        secondaryBackground: bgNext,
        child: new SingleChildScrollView(
          controller: _scrollController,
          child: new HtmlView(data: chapContent),
        ));
  }

  @override
  Widget build(BuildContext context) {
    ChapEntity currentChap = widget.listChaps.elementAt(this.currentIndex);

    return new Scaffold(
        //backgroundColor: AppConfig.paperColor,
        key: _scaffoldKey,
        appBar: new AppBar(
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.settings), onPressed: showSettingPage),
            new IconButton(
                icon: new Icon(Icons.error_outline),
                onPressed: reportErrorChap),
          ],
          title: new Text(
            currentChap.chName,
          ),
        ),
        //backgroundColor: _backgroundColor,
        //Colors.black,
        body: new NotificationListener(
            // onNotification: _onNotification,
            child: new RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: _onRefreshData,
          child: _isFetching
              ? new LoadingIndicatorWidget()
              : changeContent
                  ? buildItem(_changeSizeFont(this.prepareContent))
                  : buildItem(_changeSizeFont(this.currentContent)),
        )));
  }

  Future<Null> _onRefreshData() {
    final Completer<Null> completer = new Completer<Null>();
    new Timer(const Duration(microseconds: 500), () {
      completer.complete(null);
    });
    return completer.future.then((_) {});
  }

  void previousChap() {
    if (currentIndex <= 0) {
      return;
    }
    if (currentIndex <= 1) {
      this.setState(() {
        this._dismissDirection = DismissDirection.startToEnd;
      });
    } else if (this._dismissDirection != DismissDirection.horizontal) {
      this.setState(() {
        this._dismissDirection = DismissDirection.horizontal;
      });
    }
    this.setState(() {
      if (delta >= 0) {
        this.delta--;
      }
      this.currentIndex--;
      this.currentChap = widget.listChaps.elementAt(currentIndex);
    });
    widget.onChapChanged(currentChap);
    if (delta != 0) {
      this._loadContent();
    } else {
      delta--;
    }
    showAds();
//    print("tap next!");
  }

  void nextChap() {
    if (currentIndex >= widget.listChaps.length - 1) {
      return;
    }
    if (currentIndex >= widget.listChaps.length - 2) {
      this.setState(() {
        this._dismissDirection = DismissDirection.endToStart;
      });
    } else if (this._dismissDirection != DismissDirection.horizontal) {
      this.setState(() {
        this._dismissDirection = DismissDirection.horizontal;
      });
    }
    this.setState(() {
      if (delta <= 0) {
        this.delta++;
      }
      this.currentIndex++;
      this.currentChap = widget.listChaps.elementAt(currentIndex);
    });
    //Function.apply(widget.onChapChanged, [this.currentChap]);
    widget.onChapChanged(currentChap);

    if (delta != 0) {
      this._loadContent();
    } else {
      delta++;
    }
    showAds();
//    print("tap previous!");
  }

  void showSettingPage() {
    Navigator.of(context).push(new MaterialPageRoute(
      builder: (context) {
        return new SettingPage();
      },
    ));
  }

  void reportErrorChap() {
    showDialog(
        context: context,
        builder: (context) {
          return new SimpleDialog(
            contentPadding: EdgeInsets.all(10.0),
//            titlePadding: EdgeInsets.only(bottom: 10.0),
            title: new Text(AppConfig.langDefine.DIALOG_REPORT_CHAP_TITLE),

            children: <Widget>[
              new TextField(
                onChanged: (content) {
                  messageError = content;
                },
                autofocus: true,
                decoration: InputDecoration(
                  hintText: '<Email>:<Nội dung>',
                ),
              ),
              new Divider(),
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(Icons.send),
                    onPressed: _reportChapError,
                    alignment: AlignmentDirectional.bottomEnd,
                  )
                ],
              )
            ],
          );
        }).then((value) {});
  }

  void _reportChapError() {
    String message =
        "{\"chapid\":${this.widget.currentChap.chid}, \"storyid\":${this.widget.storyId}, \"message\":\"${this.messageError}\"}";
    NovelRequest.instance.reportError(message);
    Navigator.of(context).pop();
  }

  @override
  void dispose() {
    NovelFirebaseAdMob.instance.removeBanner();
    super.dispose();
  }

  void showAds() {
    _countChapRead++;

    if (this._countChapRead > AppConfig.readChapAds) {
      NovelFirebaseAdMob.instance.showBanner();
    }
  }
}
