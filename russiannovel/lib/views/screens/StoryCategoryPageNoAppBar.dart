import 'package:flutter/material.dart';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/models/entity/GenreEntity.dart';
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
import 'package:russiannovel/views/component/LoadingIndicatorWidget.dart';
import 'package:russiannovel/views/component/StoryCardRow.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'dart:async';

class StoryCategoryPageNoAppBar extends StatefulWidget {
  final GenreEntity genreEntity;

  StoryCategoryPageNoAppBar(
      {Key key, @required this.genreEntity})
      : super(key: key);

  @override
  _StoryCategoryPageNoAppBarState createState() =>
      new _StoryCategoryPageNoAppBarState();
}

class _StoryCategoryPageNoAppBarState extends State<StoryCategoryPageNoAppBar> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<StoryMiniEntity> listStoryMiniCategory = new List();
  final ScrollController _scrollController = new ScrollController();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  bool _isFetching = false;

  @override
  void initState() {
    super.initState();
    _init();
  }

  void _init() {
    _loadData();
    NovelFirebaseAnalytic.setCurrentScreen(
        "StoryCategoryPage", widget.genreEntity.title);
  }

  void _loadData() {
    this.setState(() => this._isFetching = true);

    NovelRequest.instance.getNovelByCategory(widget.genreEntity.key, 0, 100,
        (response) {
      setState(() {
        this._isFetching = false;
        this.listStoryMiniCategory = response.data.stories;
        print("load size storyminiInfo- category ${widget.genreEntity
                .title} : ${listStoryMiniCategory.length}");
      });
    }, (error) {
      // error
      this.setState(() => this._isFetching = false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new NotificationListener(
        onNotification: _onNotification,
        child: new RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: _onRefreshData,
          child: _isFetching
              ? new LoadingIndicatorWidget()
              : new ListView(
                  padding: const EdgeInsets.all(1.0),
                  controller: _scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  children: listStoryMiniCategory
                      .map((StoryMiniEntity storyMiniEntity) {
                    return new StoryCardRow(
                        story: storyMiniEntity, scaffoldKey: _scaffoldKey);
                  }).toList()),
        ),
      ),
    );
  }

  Future<Null> _onRefreshData() {
    final Completer<Null> completer = new Completer<Null>();
    print("on RefeshData");
    return completer.future.then((_) {});
  }

  bool _onNotification(ScrollNotification notification) {
    //print(notification);
    if (notification is OverscrollNotification) {
      if (_scrollController.offset > 0) {
        print("load more");
      }

      //return true;
    }
    // if(notification is ScrollEndNotification){
    //   print(notification);
    // }

    // if (notification is ScrollUpdateNotification) {

    // }

    return true;
  }
}
