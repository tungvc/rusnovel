import 'package:flutter/material.dart';
import 'package:russiannovel/providers/SQLDataAccess/RecentStoryDA.dart';
import 'package:russiannovel/views/component/LoadingIndicatorWidget.dart';
import 'package:russiannovel/views/component/RecentStoryCard.dart';
import 'package:russiannovel/providers/NovelFirebaseAnalytic.dart';
import 'dart:async';

class RecentStoryPage extends StatefulWidget {
  RecentStoryPage({
    Key key,
  }) : super(key: key);

  @override
  _RecentPageState createState() => new _RecentPageState();
}

class _RecentPageState extends State<RecentStoryPage> {
  bool _isLoading = true;
  final _listStory = <Map>[];

  void _getDataSuccess(List<Map> list) {
    this.setState(() {
      this._listStory.addAll(list);
      this._isLoading = false;
    });
  }

  Future<void> _load() async {
    RecentStoryDA.instance.selectRecentStory().then((list) {
      _getDataSuccess(list);
    }).catchError((err) {
      print("select recent err: $err");
      this.setState(() {
        this._isLoading = false;
      });
    });
  }

  Widget _buildListRecentStory() {
    final emptyText = new Center(
        child: new Column(
          children: <Widget>[
            new Text(
              "=_=",
              style: Theme.of(context).textTheme.body1.copyWith(
                fontSize: 100.0,
              ),
            ),
            new Text("\nEmpty")
          ],
        ));

    final listView = new ListView(
      children: _listStory.map((Map recentMap) {
        return new RecentStoryCard(
          story: recentMap,
        );
      }).toList(),
    );

    return new Container(
      child: _isLoading
          ? new LoadingIndicatorWidget()
          : _listStory.isEmpty ? emptyText : listView,
    );
  }

  @override
  void initState() {
    super.initState();
    _load();
    NovelFirebaseAnalytic.setCurrentScreen(
        "RecentStoryPage", "RecentStoryPage");
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: _buildListRecentStory(),
    );
  }
}
