import 'package:flutter/material.dart';
import 'dart:async';
import 'package:russiannovel/providers/NovelRequest.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:url_launcher/url_launcher.dart';

class StoryReadTry extends StatefulWidget {
  final int storyID;
  final int chapID;

  StoryReadTry({this.storyID, this.chapID});

  @override
  State<StatefulWidget> createState() {
    return new StoryReadTryState();
  }
}

class StoryReadTryState extends State<StoryReadTry> {
  String des = "";

  static final Map<String, String> parseReference = {
    "<p />": "\n  ",
    "<p/>": "\n  ",
    "<br>": "\n  ",
    "<br />": "\n  ",
    "&apos;": "'",
  };

  String _parseContent(String contentRaw) {
    String ret = contentRaw;
    //print('_parseContent raw $contentRaw');
    if (contentRaw != null) {
      parseReference.forEach((key, value) {
        //print('$key and $value');
        ret = ret.replaceAll(key, value);
      });
    }

    ret = ret.substring(0, 1500) + " ...";
    //print('_parseContent ret: $ret');
    return ret;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    NovelRequest.instance.getChapContent(widget.storyID, widget.chapID,
        (response) {
      setState(() {
        des = _parseContent(response.data.content);
      });
    }, (error) {});
  }

  Future<Null> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget readtry = new SingleChildScrollView(
        child: new Column(
      children: <Widget>[
        new Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
          child: new Text(
            "READ TRY:",
            softWrap: true,
            style: new TextStyle(fontSize: AppConfig.fontSize),
          ),
        ),
        new Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
          child: new Text(
            des,
            softWrap: true,
            style: new TextStyle(fontSize: AppConfig.fontSize),
          ),
        ),
        new FlatButton(
          child: new Text(
            "READ NOVEL NOW",
          ),
          onPressed: () {
            _launchInBrowser(
                'http://novelstore.info/story/detail/${widget.storyID}/novel');
                
          },
        ),
      ],
    ));

    return new Padding(
      padding:
          const EdgeInsets.only(left: 5.0, top: 10.0, right: 6.0, bottom: 5.0),
      child: new Container(
        child: new Column(
          children: <Widget>[readtry],
        ),
      ),
    );
  }
}
