import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/models/enums/ScreenEnums.dart';
import 'package:russiannovel/utils/TextConstantUtils.dart';
import 'package:russiannovel/views/screens/CategoryPage.dart';
import 'package:russiannovel/views/screens/ScreenManager.dart';
import 'package:russiannovel/views/screens/SettingPage.dart';

class MenuPart {
  final Widget icon;
  final Widget label;
  final Function onPress;

  MenuPart({this.icon, this.label, this.onPress});
}

class CustomFloatActionButton extends StatefulWidget {
  final ScreenManagerState screenManager;

  const CustomFloatActionButton({Key key, this.screenManager})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _CustomFloatActionButtonState();
  }
}

class _CustomFloatActionButtonState extends State<CustomFloatActionButton>
    with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = new AnimationController(
        vsync: this, duration: const Duration(milliseconds: 100));
  }

  @override
  Widget build(BuildContext context) {
    final List<MenuPart> menuParts = [
      new MenuPart(
          icon: new Icon(Icons.home, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_HOME_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            widget.screenManager.changeScreen(ScreenEnums.HomeScreen);
          }),
      new MenuPart(
          icon: new Icon(Icons.star, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_SUBSCRIBE_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            widget.screenManager.changeScreen(ScreenEnums.SubscribeScreen);
          }),
      new MenuPart(
          icon: new Icon(Icons.arrow_downward, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_DOWNLOADED_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            widget.screenManager.changeScreen(ScreenEnums.DownloadedScreen);
          }),
      new MenuPart(
          icon: new Icon(FontAwesomeIcons.tags, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_CATEGORY_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (context) {
                return new CategoryPage();
              },
            ));
          }),
      new MenuPart(
          icon: new Icon(Icons.settings, color: Colors.black87),
          label: new Text(AppConfig.langDefine.DRAWER_SETTING_LABEL_TEXT),
          onPress: () {
            _controller.reverse();
            Navigator.of(context).push(new MaterialPageRoute(
              builder: (context) {
                return new SettingPage();
              },
            ));
          }),
//      new MenuPart(
//          icon: new Icon(Icons.settings),
//          label: new Text(TextConstantUtils.getText(
//              TextConstantEnums.DRAWER_HOME_LABEL_TEXT)),
//          onPress: () {
//            Navigator.pop(context);
//            widget.screenManager.changeScreen(ScreenEnums.HomeScreen);
//          }),
    ];

    Color backgroundColor = Colors.white;
//    Color foregroundColor = Colors.red;

    return new Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: new List.generate(menuParts.length, (int index) {
        Widget child = new Container(
          height: 60.0,
          width: 150.0,
          alignment: FractionalOffset.topRight,
          child: new ScaleTransition(
            scale: new CurvedAnimation(
              parent: _controller,
              curve: new Interval(0.0, 1.0 - index / menuParts.length / 2.0,
                  curve: Curves.bounceInOut),
            ),
            child: new GestureDetector(
              onTap: menuParts[index].onPress,
              child: new Container(
                decoration: new BoxDecoration(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  borderRadius: new BorderRadius.all(new Radius.circular(50.0)),
                ),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    menuParts[index].label,
                    new FloatingActionButton(
                      heroTag: null,
                      backgroundColor: backgroundColor,
                      mini: true,
                      child: menuParts[index].icon,
                      onPressed: menuParts[index].onPress,
//                      onPressed: menuParts[index].onPress,
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
        return child;
      }).toList()
        ..add(
          new FloatingActionButton(
            heroTag: null,
            child: new AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget child) {
                return new Transform(
                  transform:
                      new Matrix4.rotationZ(_controller.value * 0.5 * pi),
                  alignment: FractionalOffset.center,
                  child: new Icon(
                      _controller.isDismissed ? Icons.add : Icons.close),
                );
              },
            ),
            onPressed: () {
              if (_controller.isDismissed) {
                _controller.forward();
              } else {
                _controller.reverse();
              }
            },
          ),
        ),
    );
  }
}
