import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:russiannovel/config/AppConfig.dart';
import "package:russiannovel/models/entity/StoryMiniEntity.dart";
import 'package:russiannovel/utils/StringUtils.dart';
import 'package:russiannovel/views/component/StoryCardBase.dart';
import 'package:url_launcher/url_launcher.dart';

class StoryCardColumn extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final StoryMiniEntity story;
  final double widthStoryCard;

  const StoryCardColumn({this.story, this.scaffoldKey, this.widthStoryCard});

  @override
  State<StatefulWidget> createState() =>
      new _StoryCardColumnState(story, scaffoldKey, widthStoryCard);
}

class _StoryCardColumnState extends StoryCardState {
  double widthStoryCard;
  String chapNews = "";

  _StoryCardColumnState(StoryMiniEntity entity, GlobalKey<ScaffoldState> key,
      double widthStoryCard) {
    this.data = entity;
    this.scaffoldKey = key;
    this.widthStoryCard = widthStoryCard;
    if (!AppConfig.lockAPP) {
      chapNews = StringUtils.getShortString(data.news, 16);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    loadImage(data.urlt);
  }

  Future<Null> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      throw 'Could not launch $url';
    }
  }

  void tabNovel(Widget imageCover) {
    if (this.data.refurl == null || this.data.refurl.isEmpty) {
      showNovelDetail(imageCover);
    } else {
      // ref app
      _launchInBrowser(this.data.refurl);
    }
  }

  @override
  Widget build(BuildContext context) {
    ImageProvider imageCover = new AssetImage('assets/dcover.png');
    if (data.urlt != null && data.urlt.isNotEmpty) {
      imageCover = new CachedNetworkImageProvider(this.data.urlt);
    }

    return Padding(
      padding: const EdgeInsets.all(2.5),
      child: new GestureDetector(
          onTap: () {
            tabNovel(new Image(image: imageCover));
          },
          child: Hero(
            tag: "storyHero${this.data.cid}",
            child: new Container(
              decoration: new BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                image:
                    new DecorationImage(image: imageCover, fit: BoxFit.cover),
              ),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Expanded(
                    flex: 2,
                    child: new Container(),
                  ),
                  new Container(
                    width: 10000.0,
                    height: 50.0,
                    decoration: new BoxDecoration(
                      color: new Color.fromRGBO(0, 0, 0, 0.5),
                    ),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new DefaultTextStyle(
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          style: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(color: Colors.white),
                          child: new Text(
                              StringUtils.getShortString(data.title, 30)),
                        ),
                        new DefaultTextStyle(
                            maxLines: 2,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.body2.copyWith(
                                  fontSize: 12.0,
                                  color: Colors.white70,
                                ),
                            child: new Text(this.chapNews)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
