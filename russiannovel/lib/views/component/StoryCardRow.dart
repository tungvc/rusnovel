import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import "package:russiannovel/models/entity/StoryMiniEntity.dart";
import 'package:russiannovel/views/component/StoryCardBase.dart';
import 'package:russiannovel/views/customwidgets/StoryInfoLineWidget.dart';

class StoryCardRow extends StoryCard {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final StoryMiniEntity story;

  const StoryCardRow({this.story, this.scaffoldKey});

  @override
  State<StatefulWidget> createState() =>
      new _StoryCardRowState(story, scaffoldKey);
}

class _StoryCardRowState extends StoryCardState {
  _StoryCardRowState(StoryMiniEntity entity, GlobalKey<ScaffoldState> key) {
    this.data = entity;
    this.scaffoldKey = key;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadImage(data.urlt);
  }

  @override
  Widget build(BuildContext context) {
    ImageProvider imageCover = new AssetImage('assets/dcover.png');
    if (data.urlt != null && data.urlt.isNotEmpty) {
      imageCover = new CachedNetworkImageProvider(this.data.urlt);
    }
    return GestureDetector(
        onTap: () {
          showNovelDetail(new Image(image: imageCover));
        },
        child: Hero(
          tag: "storyHero${this.data.cid}",
          child: new Card(
              child: new Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: imageCover,
                  width: 100.toDouble(),
                  height: 100.toDouble() * 1.2,
                ),
                new Expanded(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Padding(
                        padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                      ),
                      new DefaultTextStyle(
                        style: new TextStyle(
//                        fontFamily: AppConfig.fontFamily,
                          fontSize: Theme.of(context).textTheme.title.fontSize,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).textTheme.title.color,
                        ),
                        child: new Text(data.title),
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                      ),
                      new StoryInfoLineWidget(
                        title: "Lastest: ",
                        content: data.news,
                      ),
                      new StoryInfoLineWidget(
                        title: "Last Update: ",
                        content: formatDate(
                            new DateTime.fromMillisecondsSinceEpoch(
                                data.ltupdate * 1000),
                            [mm, '-', dd, '-', yyyy]),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )),
        ));
  }
}
