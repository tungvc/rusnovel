import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import "package:russiannovel/models/entity/StoryMiniEntity.dart";
import 'package:russiannovel/providers/NetworkImageLoader.dart';
import 'package:russiannovel/views/screens/StoryInfoPage.dart';

class StoryCard extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final StoryMiniEntity story;

  const StoryCard({this.story, this.scaffoldKey});

  @override
  State<StatefulWidget> createState() {
    return new StoryCardState();
  }
}

class StoryCardState extends State<StatefulWidget> {
  GlobalKey<ScaffoldState> scaffoldKey;
  StoryMiniEntity data;

  StoryCardState({this.data, this.scaffoldKey});

//  bool _isFetching = false;
  Image imgCover = new Image.asset('assets/dcover.png');
  var imgCoverProvider = new Image.asset('assets/dcover.png').image;

  Future<Uint8List> loadImage(String url) async {
    try {
      var netImg = new NetworkImageLoader(url);
      var res = await netImg.load();
      try {
        setState(() {
          imgCoverProvider = new MemoryImage(res);
          imgCover = new Image(
            image: new MemoryImage(res),
            height: 100.0,
            width: 100.0,
            fit: BoxFit.contain,
          );
        });
      } catch (ex) {}
    } on Exception {
      print("load image error");
    }
    return null;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void showNovelDetail(Widget imageCover) {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return new StoryInfoPage(
        storyMiniEntity: data,
        imageCover: imageCover,
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return null;
  }
}
