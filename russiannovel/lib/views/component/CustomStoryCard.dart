import 'package:flutter/material.dart';

class CustomStoryCard extends StatelessWidget {
  final String title;
  final Widget imageCover;
  final List<TextSpan> content;
  final Function onTap;

  const CustomStoryCard(
      {Key key, this.title, this.content, this.onTap, this.imageCover})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return GestureDetector(
        onTap: () {
          this.onTap();
        },
        child: new Card(
            child: new Padding(
          padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              imageCover,
              new Expanded(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Padding(
                      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                    ),
                    new DefaultTextStyle(
                      style: Theme.of(context).textTheme.title.copyWith(
//                        fontFamily: AppConfig.fontFamily,
                        fontSize: Theme.of(context).textTheme.title.fontSize,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).textTheme.title.color,
                      ),
                      child: new Text(this.title),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                    ),
                    new RichText(
                        text: new TextSpan(
                      style: new TextStyle(
                        color: const Color(0xFF8E8E93),
                      ),
                      children: this.content,
                    )),
                  ],
                ),
              )
            ],
          ),
        )));
  }
}
