import 'package:flutter/material.dart';

class LoadingIndicatorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new CircularProgressIndicator(
        backgroundColor: Theme.of(context).accentColor,
      ),
    );
  }

}