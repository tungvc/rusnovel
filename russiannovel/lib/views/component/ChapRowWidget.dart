import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:russiannovel/models/entity/ChapEntity.dart';
import 'package:russiannovel/providers/SQLDataAccess/NovelKVDA.dart';

typedef BoolCallback = bool Function();

class ChapRowWidget extends StatefulWidget {
  final int storyID;
  final ChapEntity entity;
  final VoidCallback onPressed;
  final VoidCallback onDownloadChap;
  ChapRowWidget(
      {this.storyID, this.entity, this.onPressed, this.onDownloadChap});

  @override
  State<StatefulWidget> createState() {
    return new ChapRowWidgetState();
  }
}

class ChapRowWidgetState extends State<ChapRowWidget> {
  
  void checkDownloaded() async {
    // bool isD = widget.entity.downloaded;
    NovelKVDA.instance
        .selectByIdAndType("CHAP_" + widget.entity.chid.toString(),
            "STORY_" + widget.storyID.toString())
        .then((NovelKVData data) {
      if (data != null) {
        this.setState(() {
          widget.entity.downloadStatus = 2; // downloaded
        });
      }
    });
    
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkDownloaded();
  }

  @override
  Widget build(BuildContext context) {
    final timeString = formatDate(
        new DateTime.fromMillisecondsSinceEpoch(widget.entity.chTime * 1000),
        [mm, '-', dd, '-', yyyy]);

    IconButton downloadStatus ;
    final downloadingButton = new IconButton(
      icon: new Icon(Icons.more_horiz, size: 20.9, color: Colors.orange),
      onPressed: null,
    );
    IconButton dowloadButton = new IconButton(
      icon: new Icon(
        Icons.done,
        size: 20.9,
        color: Colors.grey,
      ),
      onPressed: () {
        
        setState(() {
          downloadStatus = downloadingButton;
        });
        this.widget.onDownloadChap();
      },
    );
    final dowloadedButton = new IconButton(
      icon: new Icon(Icons.done, size: 20.9, color: Colors.orange),
      onPressed: null,
    );

    if(widget.entity.downloadStatus == 1){
      downloadStatus = downloadingButton;
    }else if(widget.entity.downloadStatus == 2){
      downloadStatus = dowloadedButton;
    }else{
      downloadStatus = dowloadButton;
    }
    

    return new ListTile(
      title: new Container(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Flexible(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    widget.entity.chName,
                    //style: new TextStyle(
                      //color: Colors.blueGrey,
                      //fontWeight: FontWeight.bold,
                    //),
                  )
                ],
              ),
            ),
            new Container(
              child: new Row(
                children: <Widget>[
                  new Text(
                    timeString,
                    style: new TextStyle(
                        fontStyle: FontStyle.italic,
                        color: const Color(0xFF8E8E93)),
                  ),
                  downloadStatus,
                ],
              ),
            )
          ],
        ),
      ),
      onTap: () {
        this.setState(() {
          widget.entity.downloadStatus = 2;
        });
        widget.onPressed();
        //new AlertDialog(content: new Text("data"));
      },
    );
  }
}
