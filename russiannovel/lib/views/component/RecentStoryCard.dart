import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/views/component/CustomStoryCard.dart';
import 'package:russiannovel/views/component/StoryCardBase.dart';
import "package:russiannovel/models/entity/StoryMiniEntity.dart";

class RecentStoryCard extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Map story;

  const RecentStoryCard({this.story, this.scaffoldKey});

  @override
  State<StatefulWidget> createState() =>
      new _RecentStoryCardState(story, scaffoldKey);
}

class _RecentStoryCardState extends StoryCardState {
  Map story;

  _RecentStoryCardState(Map entity, GlobalKey<ScaffoldState> key) {
    this.story = entity;

    this.scaffoldKey = key;
    this.data = new StoryMiniEntity(cid: story['cid'], title: story['title']);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<TextSpan> content = <TextSpan>[
      new TextSpan(
        text: "Reading: ",
        style: Theme.of(context).textTheme.caption.copyWith(
            fontWeight: FontWeight.bold,
            color: Theme.of(context).textTheme.title.color),
      ),
      new TextSpan(text: story['chapTitle']),
      new TextSpan(
          text: "\nTime: ",
          style: Theme.of(context).textTheme.caption.copyWith(
              fontWeight: FontWeight.bold,
              color: Theme.of(context).textTheme.title.color)),
      //color: AppConfig.useLightTheme ? Colors.black : Colors.white70)),
      new TextSpan(
          text: formatDate(
              new DateTime.fromMillisecondsSinceEpoch(story['ltRead']),
              [mm, '-', dd, '-', yyyy])),
    ];

    if (AppConfig.lockAPP) {
      content = <TextSpan>[
        new TextSpan(
            text: "\nTime: ",
            style: Theme.of(context).textTheme.body1.copyWith(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).accentColor)),
        new TextSpan(
            text: formatDate(
                new DateTime.fromMillisecondsSinceEpoch(story['ltRead']),
                [mm, '-', dd, '-', yyyy])),
      ];
    }
    ImageProvider imageCover = new AssetImage('assets/dcover.png');
    if (data.urlt != null && data.urlt.isNotEmpty) {
      imageCover = new CachedNetworkImageProvider(this.data.urlt);
    }
    return new CustomStoryCard(
      onTap: () {
        //print("tap on ${data.title}");

        this.showNovelDetail(null);
      },
      imageCover: new Image(
        image: imageCover,
        width: 100.toDouble(),
        height: 100.toDouble() * 1.2,
      ),
      title: story['title'],
      content: content,
    );
  }
}
