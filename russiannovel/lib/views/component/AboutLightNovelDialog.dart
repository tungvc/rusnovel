import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/utils/TextConstantUtils.dart';
import 'package:url_launcher/url_launcher.dart';

class LinkTextSpan extends TextSpan {
  LinkTextSpan({TextStyle style, String url, String text})
      : super(
            style: style,
            text: text ?? url,
            recognizer: new TapGestureRecognizer()
              ..onTap = () async {
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              });
}

class AboutLightNovel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final TextStyle aboutTextStyle = themeData.textTheme.body2;

    return new AboutLightNovelDialog(
        icon: new Icon(Icons.info),
        applicationVersion: 'Version 1.0.1',
        //version
        applicationIcon: new Image.asset(
          'assets/dcover.png',
          width: 100.0,
          height: 100.0,
          alignment: Alignment.centerLeft,
        ),
        applicationLegalese: '© 2018 ',
        aboutBoxChildren: <Widget>[
          new Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: new RichText(
                  text: new TextSpan(children: <TextSpan>[
                new TextSpan(
                    style: aboutTextStyle,
                    text: AppConfig.langDefine.DIALOG_ABOUT_CONTENT_TEXT),
                new TextSpan(
                    style: aboutTextStyle,
                    text:
                        '\n\n${AppConfig.langDefine.DIALOG_ABOUT_CONTACT_TEXT}'),
              ])))
        ]);
  }
}

class AboutLightNovelDialog extends StatelessWidget {
  final Widget icon;
  final String applicationName;
  final String applicationVersion;
  final Widget applicationIcon;
  final String applicationLegalese;
  final List<Widget> aboutBoxChildren;

  const AboutLightNovelDialog(
      {Key key,
      this.icon,
      this.applicationVersion,
      this.applicationIcon,
      this.applicationLegalese,
      this.aboutBoxChildren,
      this.applicationName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new ListTile(
      leading: icon,
      title: applicationName ??
          new Text(MaterialLocalizations.of(context).aboutListTileTitle(
              applicationName ?? _defaultApplicationName(context))),
      onTap: () {
        showAboutDialog(
            context: context,
            applicationName: applicationName,
            applicationVersion: applicationVersion,
            applicationIcon: applicationIcon,
            applicationLegalese: applicationLegalese,
            children: aboutBoxChildren);
      },
    );
  }
}

class AboutDialog extends StatelessWidget {
  const AboutDialog({
    Key key,
    this.applicationName,
    this.applicationVersion,
    this.applicationIcon,
    this.applicationLegalese,
    this.children,
  }) : super(key: key);

  final String applicationName;
  final String applicationVersion;
  final Widget applicationIcon;
  final String applicationLegalese;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    final String name = applicationName ?? _defaultApplicationName(context);
    final String version = applicationVersion;
    final Widget icon = applicationIcon;
    List<Widget> body = <Widget>[];
    if (icon != null)
      body.add(
          new IconTheme(data: const IconThemeData(size: 48.0), child: icon));
    body.add(new Expanded(
        child: new Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: new ListBody(children: <Widget>[
              new Text(name, style: Theme.of(context).textTheme.headline),
              new Text(version, style: Theme.of(context).textTheme.body1),
              new Container(height: 18.0),
              new Text(applicationLegalese ?? '',
                  style: Theme.of(context).textTheme.caption)
            ]))));
    body = <Widget>[
      new Row(crossAxisAlignment: CrossAxisAlignment.start, children: body),
    ];
    if (children != null) body.addAll(children);
    return new AlertDialog(
        content: new SingleChildScrollView(
          child: new ListBody(children: body),
        ),
        actions: <Widget>[
          new Row(
            children: <Widget>[
              new FlatButton(
                  child: new Text(AppConfig.langDefine.DIALOG_CONTACT_US_BUTTON_TEXT),
                  onPressed: () async {
                    String url = AppConfig.langDefine.EMAIL_FEEDBACK;
                    if (await canLaunch(url)) {
                      await launch(url);
                    } else {
                      throw 'Could not launch';
                    }
                  }),
              new FlatButton(
                  child: new Text(AppConfig.langDefine.DIALOG_LEAVE_COMMENT_BUTTON_TEXT),
                  onPressed: () async {
                    try {
                      await LaunchReview.launch();
                    } catch (ex) {
                      print(ex);
                      String url = AppConfig.langDefine.EMAIL_FEEDBACK;

                      if (await canLaunch(url)) {
                        await launch(url);
                      } else {
                        throw 'Could not launch';
                      }
                    }
                  }),
              new FlatButton(
                  child: new Text(AppConfig.langDefine.DIALOG_CLOSE_BUTTON_TEXT),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
            ],
          )
        ]);
  }
}

void showAboutDialog({
  @required BuildContext context,
  String applicationName,
  String applicationVersion,
  Widget applicationIcon,
  String applicationLegalese,
  List<Widget> children,
}) {
  showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return new AboutDialog(
          applicationName: applicationName,
          applicationVersion: applicationVersion,
          applicationIcon: applicationIcon,
          applicationLegalese: applicationLegalese,
          children: children,
        );
      });
}

String _defaultApplicationName(BuildContext context) {
  final Title ancestorTitle = context.ancestorWidgetOfExactType(Title);
  return ancestorTitle?.title ??
      Platform.resolvedExecutable.split(Platform.pathSeparator).last;
}
