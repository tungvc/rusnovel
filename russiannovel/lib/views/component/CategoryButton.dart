import 'package:flutter/material.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/models/entity/GenreEntity.dart';

class CategoryButton extends StatelessWidget {
  final GenreEntity entity;
  final VoidCallback onPressed;

  CategoryButton({this.entity,this.onPressed});

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
        child: new Text(
          entity.title,
          textAlign: TextAlign.center,
          style: new TextStyle(fontWeight: FontWeight.bold),
        ),
        color: AppConfig.useLightTheme? Colors.white:Colors.black38,
        onPressed: () {
          onPressed();
         
        });
  }
}
