import 'dart:async';
import 'dart:math';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:russiannovel/config/AppConfig.dart';
import "package:russiannovel/models/entity/StoryEntity.dart";
import 'package:russiannovel/models/entity/StoryMiniEntity.dart';
import 'package:russiannovel/providers/NetworkImageLoader.dart';
import 'package:russiannovel/providers/NovelFirebaseAdMob.dart';
import 'package:russiannovel/providers/SQLDataAccess/ChapDownloadProcessDA.dart';
import 'package:russiannovel/providers/SQLDataAccess/DownloadedDA.dart';
import 'package:russiannovel/providers/StaticData.dart';
import 'package:russiannovel/utils/DateTimeUtils.dart';
import 'package:russiannovel/utils/StringUtils.dart';
import 'package:russiannovel/views/component/LoadingIndicatorWidget.dart';
import 'package:russiannovel/views/customwidgets/StoryInfoLineWidget.dart';

enum DialogAction {
  cancel,
  discard,
  disagree,
  agree,
}

class StoryInformationWidget extends StatefulWidget {
  final int storyId;
  final StoryEntity storyEntity;
  final Widget imageCover;

  StoryInformationWidget({this.storyId, this.storyEntity, this.imageCover});

  @override
  State<StatefulWidget> createState() {
    return new _InformationState();
  }
}

class _InformationState extends State<StoryInformationWidget> {
  bool _fullDes = false;
  String buttonText = AppConfig.langDefine.STORY_INFO_READ_MORE_BUTTON_TEXT;
  bool disableDownAll = false;
  String statusDownload = "Download";
  ImageProvider imgCover = new AssetImage('assets/dcover.png');

  var rate = "";
  var view = 0;

  Future<Uint8List> loadImage(String url) async {
    try {
      var netImg = new NetworkImageLoader(url);
      var res = await netImg.load();
      setState(() {
        imgCover = new MemoryImage(res);
      });
    } on Exception {
      print("load image error");
    }
    return null;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var ran = new Random();
    this.rate = (3 + ran.nextInt(2) + ran.nextDouble()).toStringAsFixed(2);
    this.view = 6000 + ran.nextInt(10000);
    NovelFirebaseAdMob.instance.fnListenerClientEvent = downstory;
    NovelFirebaseAdMob.instance.fnListenerFailedToShow = failToLoadAds;
    if (widget.imageCover == null) {
      loadImage(widget.storyEntity.urlCover);
    }
    DownloadedDA.instance.beDownloaded(widget.storyId).then((bool downloaded) {
      if (downloaded) {
        this.setState(() {
          this.statusDownload = "Download Again";
        });
      }
    });
  }

  void reloadStatus() {
    DownloadedDA.instance.beDownloaded(widget.storyId).then((bool downloaded) {
      if (downloaded) {
        this.setState(() {
          this.statusDownload = "Download Again";
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    String des = "";

    final _rateAndSaw = new Container(
      padding:
          new EdgeInsets.only(left: 70.0, right: 70.0, top: 5.0, bottom: 5.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Container(
            child: new Row(
              children: <Widget>[
                new Icon(
                  Icons.star,
                  size: 40.0,
                  color: Colors.yellow,
                ),
                new Text("  " + rate.toString()),
              ],
            ),
          ),
          new Container(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                new Icon(
                  Icons.remove_red_eye,
                  size: 40.0,
                ),
                new Text("   " + view.toString()),
              ],
            ),
          ),
        ],
      ),
    );

    final _author = new StoryInfoLineWidget(
      title: AppConfig.langDefine.STORY_INFO_AUTHOR_LABEL_TEXT + ": ",
      content: widget.storyEntity.author,
    );

    final _category = new StoryInfoLineWidget(
      title: AppConfig.langDefine.STORY_INFO_CATEGORY_LABEL_TEXT + ": ",
      content: widget.storyEntity.category.replaceAll(",", ", "),
    );

    final _timeAgo = new StoryInfoLineWidget(
      title: AppConfig.langDefine.STORY_INFO_LAST_UPDATE_LABEL_TEXT + ": ",
      content: DateTimeUtils.getTimePass(widget.storyEntity.ltUpdate),
    );

    final _desc = new StoryInfoLineWidget(
      title: AppConfig.langDefine.STORY_INFO_DESCRIPTION_LABEL_TEXT + ": ",
      content: "",
    );

    final heroTag = "storyHero${widget.storyId}";

    final image = (widget.imageCover) == null
        ? new CachedNetworkImage(
            imageUrl: widget.storyEntity.urlCover,
            placeholder: new Image.asset(
              'assets/dcover.png',
              height: AppConfig.defaultImageWidth * 2,
              width: AppConfig.defaultImageWidth * 1.2,
            ),
            errorWidget: new Image.asset(
              'assets/dcover.png',
              height: AppConfig.defaultImageWidth * 2,
              width: AppConfig.defaultImageWidth * 1.2,
            ),
          )
        : new Container(
            height: AppConfig.defaultImageWidth * 2,
            width: AppConfig.defaultImageWidth * 1.2,
            child: widget.imageCover,
          );

    Widget storyInfo = new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        new Hero(
            tag: heroTag,
            child: new Container(
              child: image,
              alignment: Alignment.center,
            )),
        new Container(
          padding: new EdgeInsets.only(top: 5.0, bottom: 5.0),
          alignment: Alignment.center,
          child: new Text(
            widget.storyEntity.title,
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .headline
                .copyWith(fontWeight: FontWeight.bold),
          ),
        ),
        _rateAndSaw,
        new Divider(),
        _author,
        new Divider(),
        _category,
        new Divider(),
        _timeAgo,
        new Divider(),
        _desc,
      ],
    );
//    Widget storyInfo_bk = new Row(
//      crossAxisAlignment: CrossAxisAlignment.start,
//      children: <Widget>[
//        new Container(
//            child: new Image(
//          image: imgCover,
//          height: 150.0,
//          width: 150.0,
//          fit: BoxFit.contain,
//        )),
//        new Expanded(
//          child: new Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            mainAxisAlignment: MainAxisAlignment.start,
//            children: <Widget>[
//              new Text(
//                widget.storyEntity.title,
//                style: Theme.of(context)
//                    .textTheme
//                    .headline
//                    .copyWith(fontWeight: FontWeight.bold),
//              ),
//              new Padding(
//                padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
//              ),
//              _author,
//              _category,
//              _timeAgo,
//            ],
//          ),
//        ),
//      ],
//    );

    FlatButton flatButtonDownload = new FlatButton(
      child: new Text(
        this.statusDownload,
      ),
      onPressed: () {
        this.setState(() {
          this.statusDownload = "Waitting";
          this.onDownloadAll();
        });
      },
    );

    des = _fullDes
        ? widget.storyEntity.description
        : StringUtils.getShortString(widget.storyEntity.description, 100);

    if (AppConfig.lockAPP || AppConfig.lockDOWN) {
      flatButtonDownload = null;

      des = _fullDes
          ? widget.storyEntity.description
          : StringUtils.getShortString(widget.storyEntity.description, 100);
    }
    Widget describe = new Column(
      children: <Widget>[
        new Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
          child: new Text(
            des,
            softWrap: true,
            style: new TextStyle(),
          ),
        ),
        new ButtonBar(
          children: <Widget>[
            new FlatButton(
              child: new Text(
                buttonText,
              ),
              onPressed: () {
                this.setState(() {
                  _fullDes = !_fullDes;
                  buttonText = _fullDes
                      ? AppConfig.langDefine.STORY_INFO_COLLAPSE_BUTTON_TEXT
                      : AppConfig.langDefine.STORY_INFO_READ_MORE_BUTTON_TEXT;
                });
                // Perform some action
              },
            ),
            flatButtonDownload,
          ],
        ),
      ],
    );

    return new Padding(
      padding:
          const EdgeInsets.only(left: 5.0, top: 10.0, right: 6.0, bottom: 5.0),
      child: new Container(
        child: new Column(
          children: <Widget>[storyInfo, describe],
        ),
      ),
    );
  }

  void showDialogMessage<T>({BuildContext context, Widget child}) {
    showDialog<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {
      // The value passed to Navigator.pop() or null.
      if (value != null) {}
    });
  }

  void onDownloadAll() {
    print(" proccess download");
    NovelFirebaseAdMob.instance.fnListenerClientEvent = downstory;
    NovelFirebaseAdMob.instance.fnListenerFailedToShow = null;
    NovelFirebaseAdMob.instance.loadBanner();

    Random random = Random();
    int value = random.nextInt(100);
    if (value < AppConfig.ratioDownAll) {
      downstory();
      return;
    }

    if (AppConfig.beShowAds) {
      //NovelFirebaseAdMob.instance.loadBanner(downstory);
      NovelFirebaseAdMob.instance.fnListenerClientEvent = downstory;
      NovelFirebaseAdMob.instance.fnListenerFailedToShow = failToLoadAds;
      NovelFirebaseAdMob.instance.fnListenerFailedToLoad = failToLoadAds;
      NovelFirebaseAdMob.instance.showBanner();
    }

    this.showDialogMessage<DialogAction>(
        context: context,
        child: new AlertDialog(
            title: const Text('Light Novel: Alert'),
            content: new Text(
              "Do you want to download ${widget.storyEntity.title} ? Please Click Ads to download.\n Thankui supported Team!! ",
              //style: dialogTextStyle
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('DISAGREE'),
                  onPressed: () {
                    Navigator.pop(context, DialogAction.disagree);

                    NovelFirebaseAdMob.instance.removeBanner();
                    statusDownload = "Download";
                  }),
              new FlatButton(
                  child: const Text('AGREE'),
                  onPressed: () async {
                    Navigator.pop(context, DialogAction.agree);

                    //NovelFirebaseAdMob.instance.loadBanner(downstory);

                    if (!AppConfig.beShowAds) {
                      downstory();
                    } else {}
                  })
            ]));
  }

  void failToLoadAds() {
    print("call to failToLoadAds");
    downstory();
    AppConfig.beShowAds = true;
  }

  void downstory() {
    //this.storyInformationWidget.setDownloaded();
    print(" insert chap to db ");
    DownloadedDA.instance.insert(new StoryMiniEntity(
      cid: this.widget.storyId,
      ltupdate: widget.storyEntity.ltUpdate,
      name: widget.storyEntity.name,
      news: widget.storyEntity.listChaps
          .elementAt(widget.storyEntity.listChaps.length - 1)
          .chName,
      title: widget.storyEntity.title,
      urlt: widget.storyEntity.urlCover,
    ));
    print("size add ${widget.storyEntity.listChaps.length}");
    for (var item in widget.storyEntity.listChaps) {
      ChapDownloadProcessDA.instance.insert(item.chid, this.widget.storyId);
    }

    StaticData.enableBackgroundDownload = true;
    NovelFirebaseAdMob.instance.removeBanner();
    AppConfig.beShowAds = false;

    this.setState(() {
      this.statusDownload = "Downloading";
    });
  }
}
