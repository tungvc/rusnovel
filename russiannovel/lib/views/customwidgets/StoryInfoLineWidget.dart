import 'package:flutter/material.dart';

class StoryInfoLineWidget extends StatelessWidget {
  final String title;
  final String content;

  const StoryInfoLineWidget(
      {Key key, @required this.title, @required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new RichText(
        text: new TextSpan(children: <TextSpan>[
      new TextSpan(
          text: title,
          style: Theme.of(context).textTheme.button.copyWith(
            fontWeight: FontWeight.bold,
          )),
      new TextSpan(
          text: content,
          style: new TextStyle(
            color: const Color(0xFF8E8E93),
          )),
    ]));
  }
}
