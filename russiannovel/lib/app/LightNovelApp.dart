import 'package:flutter/material.dart';
import 'package:russiannovel/config/AppConfig.dart';
import 'package:russiannovel/providers/SQLDataAccess/AppConfigDA.dart';
import 'package:russiannovel/providers/SQLProvider.dart';
import 'package:russiannovel/utils/ThemeUtils.dart';
import 'package:russiannovel/views/screens/HomePage.dart';

//import 'package:russiannovel/views/screens/Master.dart';
import 'package:russiannovel/views/screens/ScreenManager.dart';
//import 'package:scoped_model/scoped_model.dart';

// final _themeGlobalKey = new GlobalKey(debugLabel: 'app_theme');
class LightNovelApp extends StatefulWidget {
  static LightNovelApp _instance = new LightNovelApp();

  static LightNovelApp getInstance() {
    return _instance;
  }

  // This widget is the root of your application.
  LightNovelApp(
      {this.enablePerformanceOverlay: true,
      this.checkerboardRasterCacheImages: true,
      this.checkerboardOffscreenLayers: true,
      this.onSendFeedback,
      Key key})
      : super(key: key);

  final bool enablePerformanceOverlay;

  final bool checkerboardRasterCacheImages;

  final bool checkerboardOffscreenLayers;

  final VoidCallback onSendFeedback;

  final LightNovelAppState lightAppState = new LightNovelAppState();

  @override
  State<StatefulWidget> createState() => lightAppState;

  void changeTheme(ThemeEnums value) {
    lightAppState.changeTheme(value);
  }

  void changeTextSize(double value) {
    lightAppState.changeTextSize(value);
  }

  void changeTextColor(Color color) {
    lightAppState.changeTextColor(color);
  }

  void changeBackgroundColor(Color color) {
    lightAppState.changeBackgroundColor(color);
  }
}

class LightNovelAppState extends State<LightNovelApp> {
  ThemeEnums _theme = AppConfig.theme;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    SQLProvider.instance.close();
  }

  @override
  Widget build(BuildContext context) {
//    Widget home = new HomePage(
//      title: "Light Novel",
//    );
//    Widget home = new Master(
//      title: "Light Novel",
//      useLightTheme: true,
//      body: new HomePage(
//        title: "Light Novel",
//      ),
//    );

    Widget home = new ScreenManager(
      title: "легкая новелла",
      useLightTheme: true,
      body: new HomePage(
        title: "легкая новелла",
      ),
    );

    return new MaterialApp(
      title: 'легкая новелла',
      theme: ThemeUtils.getTheme(_theme),
      home: home,
    );
  }

  void changeTheme(ThemeEnums value) {
    this.setState(() {
      _theme = value;
      AppConfig.theme = value;
      AppConfigDA.instance.saveAppConfig();
    });
  }

  void changeTextSize(double value) {
    this.setState(() {
      AppConfig.fontSize = value;
      AppConfigDA.instance.saveAppConfig();
    });
  }

  void changeTextColor(Color color) {
    this.setState(() {
      this.setState(() {
        AppConfig.textChapReaderColor = color.value;
        AppConfigDA.instance.saveAppConfig();
      });
    });
  }

  void changeBackgroundColor(Color color) {
    this.setState(() {
      this.setState(() {
        AppConfig.backgroundChapReaderColor = color.value;
        AppConfigDA.instance.saveAppConfig();
      });
    });
  }
}
